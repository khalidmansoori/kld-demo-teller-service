#!/bin/bash
function jsonValue() {
KEY=$1
num=$2
awk -F"[,:}]" '{for(i=1;i<=NF;i++){if($i~/'$KEY'\042/){print $(i+1)}}}' | tr -d '"' | sed -n ${num}p
}

URL="http://localhost:8080"
if [ ! -z $1 ]; then
    URL="http://localhost:"$1
fi

AccountName1="Saad"
AccountName2="Ayesha"
accountNumber=""
function create_account()
{
AccountName=$1
AccountCreationAPI=$(echo "curl -XPUT" ${URL}"/create-account/")
CURL="${AccountCreationAPI}$AccountName"
echo
echo
echo "["$(date -u)"]" "Creating account for "$AccountName
echo "["$(date -u)"]" $CURL
accountNumber=$($CURL | jsonValue "accountNumber" 1)
echo "["$(date -u)"]" $AccountName " account created Number " $accountNumber
}

function check_balance()
{
AccountName=$1
accountNumber=$2
AccountBalanceAPI=$(echo "curl -XGET" ${URL}"/balance/")
echo "["$(date -u)"]" "Checking account balance for "$AccountName
CURL="${AccountBalanceAPI}$accountNumber"
echo "["$(date -u)"]" $CURL
accountBalance=$($CURL)
echo "["$(date -u)"]" $AccountName " balance " $accountBalance
if [ -z $3 ]; then
    exit 1
fi

balance=$(echo $accountBalance | jsonValue "amount" 1)
if [ "$balance" != "$3" ]; then
  echo "["$(date -u)"]" "Balance check failed for $AccountName:" $accountNumber " Actual " $balance " expected is "$3
  exit
fi
}

function deposit()
{
amount=$1
accountNumber=$2
AccountName=$3
API=$(echo "curl -XPOST" ${URL}"/deposit/")
echo "["$(date -u)"]" "Depositing amount "$amount " to "$AccountName
CURL="${API}$amount"/"$accountNumber"
echo "["$(date -u)"]" $CURL
result=$($CURL)
echo "["$(date -u)"]" $result
}


function withdraw()
{
amount=$1
accountNumber=$2
AccountName=$3
API=$(echo "curl -XPOST" ${URL}"/withdraw/")
echo "["$(date -u)"]" "Withdrawing amount "$amount " from "$AccountName
CURL="${API}$amount"/"$accountNumber"
echo "["$(date -u)"]" $CURL
result=$($CURL)
echo "["$(date -u)"]" $result
}

function transfer()
{
amount=$1
Number1=$2
Name1=$3
Number2=$4
Name2=$5
API=$(echo "curl -XPOST" ${URL}"/transfer/")
echo "["$(date -u)"]" "Transferring amount "$amount " from "$Name1 "to " $Name2
CURL="${API}$amount"/"$Number1"/""$Number2
echo "["$(date -u)"]" $CURL
result=$($CURL)
echo "["$(date -u)"]" $result
}

function check_balance_all()
{
check_balance  $AccountName1 $accountNumber1 $1 
check_balance  $AccountName2 $accountNumber2 $2
}

create_account $AccountName1
accountNumber1=$accountNumber

create_account $AccountName
accountNumber2=$accountNumber

# All Balance should be zero
check_balance_all 0.0 0.0

# deposited 10 to Saad
deposit 10 $accountNumber1 $AccountName1
check_balance  $AccountName1 $accountNumber1 10.0

# deposited 1000.0001 to Ayesha
deposit 1000.0001 $accountNumber2 $AccountName2
check_balance  $AccountName2 $accountNumber2 1000.0001

# withdraw .00011 from Ayesha, so 1000.0000
withdraw .0001 $accountNumber2 $AccountName2
check_balance  $AccountName2 $accountNumber2 1000.0000

# transfer 100 to Ayesha, however saad has 10.0 hence failed, NO Change in Balance
transfer 100 $accountNumber1 $AccountName1 $accountNumber2 $AccountName2
check_balance_all 10.0 1000.0000

# Transfer 1000 from Ayesha to Saad, SUCCESS, Ayesha looses all and Saad will be 1010.0
transfer 1000 $accountNumber2 $AccountName2 $accountNumber1 $AccountName1
check_balance_all 1010.0 0.0000

# Transfer 1000 from Saad to Saad, FAIL, NO Change in Balance
transfer 1000 $accountNumber1 $AccountName1 $accountNumber1 $AccountName1
check_balance_all 1010.0 0.0000

# withdraw 1010.0 from Saad, so 0.0
withdraw 1010.0 $accountNumber1 $AccountName1

# withdraw 1010.0 from Saad, FAIL, NO Change in Balance
withdraw 1010.0 $accountNumber1 $AccountName1

# withdraw 1010.0 from Ayesha, FAIL, NO Change in Balance
withdraw 1010.0 $accountNumber2 $AccountName2

# Both left with nothing :) Story ends.
check_balance_all 0.0 0.0000

