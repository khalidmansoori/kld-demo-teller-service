package com.kld;

import com.kld.demoteller.*;
import com.kld.demoteller.impl.AccountServiceImpl;
import com.kld.demoteller.impl.CustomerServiceImpl;
import com.kld.demoteller.impl.TellerServiceImpl;
import com.kld.demoteller.impl.TransactionServiceImpl;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Random;

import static org.junit.Assert.*;

public class TellerServiceTest {

    protected CustomerService customerService = new CustomerServiceImpl(new CustomerNumberGenerator() {
        @Override
        public CustomerNumber generate() {

            return new CustomerNumber(randomBigInt());
        }
    });

    protected AccountService accountService = new AccountServiceImpl(customerService, new AccountNumberGenerator() {
        @Override
        public AccountNumber generate() {

            return new AccountNumber(randomBigInt());
        }
    });

    protected TransactionService transactionService = new TransactionServiceImpl(new TransactionIdGenerator() {
        @Override
        public String generate() {

            Random randomGenerator = new Random();
            Integer number = randomGenerator.nextInt(5000000);
            return number.toString();
        }
    });

    protected TellerService tellerService = new TellerServiceImpl(accountService, transactionService);

    private BigInteger randomBigInt() {

        Random randomGenerator = new Random();
        Integer number = randomGenerator.nextInt(5000000);
        return new BigInteger(number.toString());
    }


    @Test
    public void TellerServiceTest() {

        assertNull(tellerService.balance(new BigInteger("12345")));
        AccountNumber saadAccount = tellerService.createAccount(new CustomerInfo("Saad"), "Test");
        AccountNumber ayeshaAccount = tellerService.createAccount(new CustomerInfo("Ayesha"), "Test");

        assertNotNull(tellerService.balance(saadAccount.getAccountNumber()));
        assertNotNull(tellerService.balance(ayeshaAccount.getAccountNumber()));

        assertEquals(new BigDecimal("0.0"), tellerService.balance(saadAccount.getAccountNumber()));
        assertEquals(new BigDecimal("0.0"), tellerService.balance(ayeshaAccount.getAccountNumber()));

        BigDecimal transferAmount = new BigDecimal("100.10");
        TransactionResult transactionResult = tellerService
                .transferMoney(saadAccount.getAccountNumber(), ayeshaAccount.getAccountNumber(), transferAmount,
                               "Test xfer");
        assertEquals(TransactionResult.DEBIT_FAILED_INSUFFICIENT_FUND, transactionResult);

        assertEquals(new BigDecimal("0.0"), tellerService.balance(saadAccount.getAccountNumber()));
        assertEquals(new BigDecimal("0.0"), tellerService.balance(ayeshaAccount.getAccountNumber()));

        transactionResult = tellerService
                .deposit(saadAccount.getAccountNumber(), new BigDecimal("10000.001"), "initial amount");
        assertEquals(TransactionResult.SUCCESS, transactionResult);

        assertEquals(new BigDecimal("10000.001"), tellerService.balance(saadAccount.getAccountNumber()));
        assertEquals(new BigDecimal("0.0"), tellerService.balance(ayeshaAccount.getAccountNumber()));

        transactionResult = tellerService.withdraw(ayeshaAccount.getAccountNumber(), new BigDecimal("100.10"), "");
        assertEquals(TransactionResult.DEBIT_FAILED_INSUFFICIENT_FUND, transactionResult);

        assertEquals(new BigDecimal("10000.001"), tellerService.balance(saadAccount.getAccountNumber()));
        assertEquals(new BigDecimal("0.0"), tellerService.balance(ayeshaAccount.getAccountNumber()));

        transactionResult = tellerService
                .transferMoney(saadAccount.getAccountNumber(), ayeshaAccount.getAccountNumber(), transferAmount,
                               "Test xfer");
        assertEquals(TransactionResult.SUCCESS, transactionResult);

        assertEquals(new BigDecimal("9899.901"), tellerService.balance(saadAccount.getAccountNumber()));
        assertEquals(new BigDecimal("100.10"), tellerService.balance(ayeshaAccount.getAccountNumber()));

        transactionResult = tellerService.withdraw(ayeshaAccount.getAccountNumber(), new BigDecimal("100.10"), "");
        assertEquals(TransactionResult.SUCCESS, transactionResult);

        transactionResult = tellerService.withdraw(saadAccount.getAccountNumber(), new BigDecimal("9899"), "");
        assertEquals(TransactionResult.SUCCESS, transactionResult);

        assertEquals(new BigDecimal("0.901"), tellerService.balance(saadAccount.getAccountNumber()));
        assertEquals(new BigDecimal("0.00"), tellerService.balance(ayeshaAccount.getAccountNumber()));
    }
}
