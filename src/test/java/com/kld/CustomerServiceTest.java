package com.kld;

import com.kld.demoteller.*;
import com.kld.demoteller.impl.CustomerServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class CustomerServiceTest {
    private CustomerService customerService;

    @Before
    public void setup() {

        customerService = new CustomerServiceImpl(new CustomerNumberGenerator() {
            @Override
            public CustomerNumber generate() {

                return new CustomerNumber(new BigInteger("123456"));
            }
        });
    }

    @Test
    public void TestCustomerService() {

        CustomerInfo customerInfo = new CustomerInfo("Ayesha");
        Customer customer = customerService.createORGetCustomer(customerInfo);
        assert (customer != null);
        assertEquals(customer.getName(), "Ayesha");
        assertEquals(customer.getCustomerId(), new CustomerNumber(new BigInteger("123456")));
    }
}
