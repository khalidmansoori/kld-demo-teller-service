package com.kld;

import com.kld.demoteller.AccountNumber;
import com.kld.demoteller.impl.AccountImpl;

import java.math.BigDecimal;
import java.util.Currency;

public class TestAccount extends AccountImpl {

    public TestAccount(AccountNumber accountNumber, BigDecimal initialBalance) {

        super(accountNumber, initialBalance);
    }

    public TestAccount(AccountNumber accountNumber) {

        super(accountNumber);
    }

    public void setCurrency(Currency currency) {

        this.currency = currency;
    }
}
