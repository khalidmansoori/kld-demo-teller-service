package com.kld;

import com.kld.demoteller.*;
import com.kld.demoteller.impl.AccountServiceImpl;
import com.kld.demoteller.impl.CustomerImpl;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class AccountServiceTest {

    protected AccountService accountService;

    @Before
    public void setup() {

        accountService = new AccountServiceImpl(new CustomerService() {
            @Override
            public Customer createORGetCustomer(CustomerInfo customerInfo) {

                return new CustomerImpl(customerInfo, new CustomerNumber(new BigInteger("12345")));
            }
        }, new AccountNumberGenerator() {
            @Override
            public AccountNumber generate() {

                return new AccountNumber(new BigInteger("12345"));
            }
        });

    }

    @Test
    public void TestAccountService() {

        accountService.createAccount(new BigDecimal("100.10"), new CustomerInfo("Saad"));
        AccountNumber accountNumber = new AccountNumber(new BigInteger("12345"));
        Account account = accountService.accountByNumber(accountNumber);
        assertEquals(account.accountNumber(), accountNumber);
        assertEquals(account.balance(), new Money(new BigDecimal("100.10")));
    }

}
