package com.kld;

import com.kld.demoteller.*;
import com.kld.demoteller.impl.AccountImpl;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Currency;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class AccountTest {

    @Test
    public void TestSimpleAccountCreation() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")));
        assertEquals(account.accountNumber().getAccountNumber(), new BigInteger("123456789"));
        assertEquals(account.currency(), Currency.getInstance(Locale.UK));
        assertEquals(account.getType(), AccountType.SAVING);
        assertEquals(account.balance(), new Money(new BigDecimal("0.0")));
    }

    @Test
    public void TestAccountCreationWithBalance() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")), new BigDecimal("1000.12345"));
        assertEquals(account.accountNumber().getAccountNumber(), new BigInteger("123456789"));
        assertEquals(account.currency(), Currency.getInstance(Locale.UK));
        assertEquals(account.getType(), AccountType.SAVING);
        assertEquals(account.balance(), new Money(new BigDecimal("1000.12345")));
    }

    @Test
    public void TestAccountCreditSimple() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")));
        TransactionResult creditResult = account.credit(new Money(new BigDecimal("45.10")));
        assertEquals(creditResult, TransactionResult.SUCCESS);
        assertEquals(new Money(new BigDecimal("45.10")).getAmount(), account.balance().getAmount());
    }

    @Test
    public void TestAccountCreditBigAmount() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")));
        Money bigAmount = new Money(new BigDecimal("123456789123456789123456789.123456789"));
        TransactionResult creditResult = account.credit(bigAmount);
        assertEquals(creditResult, TransactionResult.SUCCESS);
        assertEquals(bigAmount, account.balance());
    }

    @Test
    public void TestAccountCreditSmallAmount() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")));
        Money smallAmount = new Money(new BigDecimal("0.123456789123456789"));
        TransactionResult creditResult = account.credit(smallAmount);
        assertEquals(creditResult, TransactionResult.SUCCESS);
        assertEquals(smallAmount, account.balance());
    }

    @Test
    public void TestAccountCreditNegativeAmount() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")));
        Money negativeAmount = new Money(new BigDecimal("-0.123456789123456789"));
        Money originalMoney = account.balance();
        TransactionResult creditResult = account.credit(negativeAmount);
        assertEquals(creditResult, TransactionResult.CREDIT_FAILED_INVALID_AMOUNT);
        assertEquals(originalMoney, account.balance());
    }

    @Test
    public void TestAccountCreditZeroAmount() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")));
        Money zeroAmount = new Money(new BigDecimal("0.0000000"));
        Money originalMoney = account.balance();
        TransactionResult creditResult = account.credit(zeroAmount);
        assertEquals(creditResult, TransactionResult.CREDIT_FAILED_INVALID_AMOUNT);
        assertEquals(originalMoney, account.balance());
    }

    @Test
    public void TestAccountCreditDifferentCurrency() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")));
        Money amount = new Money(Currency.getInstance(Locale.US), new BigDecimal("1230.123456789123456789"));
        TransactionResult creditResult = account.credit(amount);
        assertEquals(creditResult, TransactionResult.CREDIT_FAILED_UNSUPPORTED_CURRENCY);
        assertEquals(new Money(), account.balance());
    }

    @Test
    public void TestAccountDebitSimple() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")), new BigDecimal("1000.12345"));
        TransactionResult debitResult = account.debit(new Money(new BigDecimal("1000")));
        assertEquals(debitResult, TransactionResult.SUCCESS);
        assertEquals(new Money(new BigDecimal("0.12345")).getAmount(), account.balance().getAmount());
    }

    @Test
    public void TestAccountDebitBigAmount() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")),
                                          new BigDecimal("123456789123456789123456789.12345"));

        TransactionResult debitResult = account.debit(new Money(new BigDecimal("123456789123456789123456789.12344")));
        assertEquals(debitResult, TransactionResult.SUCCESS);
        assertEquals(new Money(new BigDecimal("0.00001")).getAmount(), account.balance().getAmount());
    }

    @Test
    public void TestAccountDebitSmallAmount() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")), new BigDecimal("1000.12345"));
        assertEquals(account.balance(), new Money(new BigDecimal("1000.12345")));
        TransactionResult debitResult = account.debit(new Money(new BigDecimal("0.00001")));
        assertEquals(debitResult, TransactionResult.SUCCESS);
        assertEquals(new Money(new BigDecimal("1000.12344")).getAmount(), account.balance().getAmount());
    }

    @Test
    public void TestAccountDebitNegativeAmount() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")), new BigDecimal("1000.12345"));

        TransactionResult debitResult = account.debit(new Money(new BigDecimal("-1000")));
        assertEquals(debitResult, TransactionResult.DEBIT_FAILED_INVALID_AMOUNT);
        assertEquals(new Money(new BigDecimal("1000.12345")).getAmount(), account.balance().getAmount());
    }

    @Test
    public void TestAccountDebitZeroAmount() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")), new BigDecimal("1000.12345"));

        TransactionResult debitResult = account.debit(new Money(new BigDecimal("0.00")));
        assertEquals(debitResult, TransactionResult.DEBIT_FAILED_INVALID_AMOUNT);
        assertEquals(new Money(new BigDecimal("1000.12345")).getAmount(), account.balance().getAmount());
    }

    @Test
    public void TestAccountDebitDifferentCurrency() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")));
        Money amount = new Money(Currency.getInstance(Locale.US), new BigDecimal("1230.123456789123456789"));
        TransactionResult debitResult = account.debit(amount);
        assertEquals(debitResult, TransactionResult.DEBIT_FAILED_UNSUPPORTED_CURRENCY);
        assertEquals(new Money(), account.balance());
    }

    @Test
    public void TestAccountTransactions() {

        Account account = new AccountImpl(new AccountNumber(new BigInteger("123456789")));
        Money amount = new Money(new BigDecimal("1230.19"));

        assertEquals(account.transactions().isEmpty(), true);
        account.credit(amount);


        assertEquals(account.transactions().isEmpty(), false);

        TransactionRecord record = account.transactions().get(0);
        assertEquals(record.amount(), amount.getAmount());
        assertEquals(record.balance(), account.balance().getAmount());
        assertEquals(record.isCredit(), true);

        account.debit(amount);

        assertEquals(account.transactions().isEmpty(), false);
        TransactionRecord debitRecord = account.transactions().get(1);
        assertEquals(debitRecord.amount(), amount.getAmount());
        assertEquals(new BigDecimal("0.00"), account.balance().getAmount());
        assertEquals(debitRecord.isCredit(), false);

        account.credit(amount);

        Money zeroMoney = new Money(new BigDecimal("0.00"));
        account.debit(zeroMoney);

        TransactionRecord debitFailRecord = account.transactions().get(3);
        assertEquals(debitFailRecord.amount(), zeroMoney.getAmount());
        assertEquals(amount.getAmount(), account.balance().getAmount());
        assertEquals(debitRecord.isCredit(), false);

        account.credit(zeroMoney);
        TransactionRecord creditFailRecord = account.transactions().get(4);
        assertEquals(creditFailRecord.amount(), zeroMoney.getAmount());
        assertEquals(amount.getAmount(), account.balance().getAmount());
        assertEquals(debitRecord.isCredit(), false);

        account.credit(amount);
        account.credit(amount);
        account.credit(amount);
        account.credit(amount);
        account.credit(amount);
        account.credit(amount);
        account.credit(amount);

        account.debit(amount);
        account.debit(amount);
        account.debit(amount);
        account.debit(amount);
        account.debit(amount);
        account.debit(amount);
        account.debit(amount);


        TransactionRecord lastRecord = account.transactions().get(account.transactions().size() - 1);
        assertEquals(lastRecord.amount(), amount.getAmount());
        assertEquals(lastRecord.balance(), account.balance().getAmount());
        assertEquals(lastRecord.isCredit(), false);

    }

    //  private static final String ACCOUNT_ID_FIELD = "account_id";
    //  private static final String ACCOUNT_MONEY_FIELD = "balance";
    //
    //  public final Long money;
    //  public final Long accountNumber;
    //
    //  public Account(Long money, Long accountNumber) {
    //    this.money = money;
    //    this.accountNumber = accountNumber;
    //  }
    //
    //  static Account fromJson(JsonObject jsonObject) {
    //    return new Account(jsonObject.getLong(ACCOUNT_MONEY_FIELD), jsonObject.getLong(ACCOUNT_ID_FIELD));
    //  }
}
