package com.kld;


import com.kld.demoteller.Money;
import com.kld.exception.UnsupportedCurrencyException;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MoneyTest {

    @Test
    public void TestSimpleMoneyCreation() {

        Money money = new Money(new BigDecimal("1.1"));
        assertEquals(money.getAmount(), new BigDecimal("1.1"));
        assertEquals(money.getCurrency(), Currency.getInstance(Locale.UK));
    }

    @Test
    public void TestMoneyCreation() {

        Money money = new Money(Currency.getInstance(Locale.US), new BigDecimal("1.000001"));
        assertEquals(money.getAmount(), new BigDecimal("1.000001"));
        assertEquals(money.getCurrency(), Currency.getInstance(Locale.US));
    }

    @Test
    public void TestMoneyEmptyCreation() {

        Money money = new Money();
        assertEquals(money.getAmount(), new BigDecimal("0.0"));
        assertEquals(money.getCurrency(), Currency.getInstance(Locale.UK));
    }

    @Test
    public void TestMoneyCreationNull() {

        boolean pass = false;
        try {
            Money money = new Money(null);
        } catch (NullPointerException e) {
            assertTrue("Pass", true);
            pass = true;
        }

        assertEquals(pass, true);
    }


    @Test
    public void TestMoneyAddition() {

        Money currentMoney = new Money(new BigDecimal("1.1"));
        Money creditMoney = new Money(new BigDecimal("11213221312323123231.0001"));
        try {
            currentMoney.add(creditMoney);
        } catch (UnsupportedCurrencyException e) {

        }
        Money finalMoney = new Money(new BigDecimal("11213221312323123232.1001"));
        assertEquals(finalMoney.getCurrency(), currentMoney.getCurrency());
        assertEquals(finalMoney.getAmount(), currentMoney.getAmount());
        assertEquals(finalMoney, currentMoney);
    }

    @Test
    public void TestMoneyAdditionNewCurrancy() {

        Money currentMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("1.1"));
        Money creditMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("11213221312323123231.0001"));
        try {
            currentMoney.add(creditMoney);
        } catch (UnsupportedCurrencyException e) {

        }
        Money finalMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("11213221312323123232.1001"));
        assertEquals(finalMoney.getCurrency(), currentMoney.getCurrency());
        assertEquals(finalMoney.getAmount(), currentMoney.getAmount());
        assertEquals(finalMoney, currentMoney);
    }

    @Test
    public void TestMoneyUnsupportedCurrencyAddition() {

        Money currentMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.1"));
        Money debitMoney = new Money(new BigDecimal("1.001"));
        boolean pass = false;
        try {
            currentMoney.add(debitMoney);
        } catch (UnsupportedCurrencyException e) {
            pass = true;
        }

        assertEquals(pass, true);
    }

    @Test
    public void TestMoneyDifferentUnsupportedCurrencyAddition() {

        Money currentMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.1"));
        Money debitMoney = new Money(Currency.getInstance(Locale.GERMANY), new BigDecimal("1.001"));
        boolean pass = false;
        try {
            currentMoney.add(debitMoney);
        } catch (UnsupportedCurrencyException e) {
            pass = true;
        }

        assertEquals(pass, true);
    }

    @Test
    public void TestMoneySubtraction() {

        Money currentMoney = new Money(new BigDecimal("10001.1"));
        Money debitMoney = new Money(new BigDecimal("1.001"));
        try {
            currentMoney.subtract(debitMoney);
        } catch (UnsupportedCurrencyException e) {

        }
        Money finalMoney = new Money(new BigDecimal("10000.099"));
        assertEquals(finalMoney.getCurrency(), currentMoney.getCurrency());
        assertEquals(finalMoney.getAmount(), currentMoney.getAmount());
        assertEquals(finalMoney, currentMoney);
    }


    @Test
    public void TestMoneySubtractionNoEnoughMoney() {

        Money currentMoney = new Money(new BigDecimal("0.1"));
        Money debitMoney = new Money(new BigDecimal("1.001"));
        try {
            currentMoney.subtract(debitMoney);
        } catch (UnsupportedCurrencyException e) {

        }
        Money finalMoney = new Money(new BigDecimal("-0.901"));
        assertEquals(finalMoney.getCurrency(), currentMoney.getCurrency());
        assertEquals(finalMoney.getAmount(), currentMoney.getAmount());
        assertEquals(finalMoney, currentMoney);
    }

    @Test
    public void TestMoneyDifferentUnsupportedCurrencySubtract() {

        Money currentMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.1"));
        Money debitMoney = new Money(Currency.getInstance(Locale.GERMANY), new BigDecimal("1.001"));
        boolean pass = false;
        try {
            currentMoney.subtract(debitMoney);
        } catch (UnsupportedCurrencyException e) {
            pass = true;
        }

        assertEquals(pass, true);
    }

    @Test
    public void TestMoneyUnsupportedCurrencySubtract() {

        Money currentMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.1"));
        Money debitMoney = new Money(new BigDecimal("1.001"));
        boolean pass = false;
        try {
            currentMoney.subtract(debitMoney);
        } catch (UnsupportedCurrencyException e) {
            pass = true;
        }

        assertEquals(pass, true);
    }

    @Test
    public void TestMoneyNewCurrencySubtract() {

        Money currentMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.1"));
        Money debitMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("1.001"));
        try {
            currentMoney.subtract(debitMoney);
        } catch (UnsupportedCurrencyException e) {

        }
        Money finalMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10000.099"));
        assertEquals(finalMoney.getCurrency(), currentMoney.getCurrency());
        assertEquals(finalMoney.getAmount(), currentMoney.getAmount());
        assertEquals(finalMoney, currentMoney);
    }


    @Test
    public void TestMoneyLesser() {

        Money currentMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("101.1"));
        Money otherMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("1001.001"));
        boolean lesser = false;
        try {
            lesser = currentMoney.isLesser(otherMoney);
        } catch (UnsupportedCurrencyException e) {

        }

        assertEquals(lesser, true);
    }

    @Test
    public void TestMoneyLesserEqual() {

        Money currentMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("100.1"));
        Money otherMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.100"));
        boolean lesser = false;
        try {
            lesser = currentMoney.isLesserOrEqual(otherMoney);
        } catch (UnsupportedCurrencyException e) {

        }

        assertEquals(lesser, true);
        lesser = false;
        otherMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("1001.1"));
        try {
            lesser = currentMoney.isLesserOrEqual(otherMoney);
        } catch (UnsupportedCurrencyException e) {

        }

        assertEquals(lesser, true);
    }

    @Test
    public void TestMoneyLesserDifferentCurrency() {

        Money currentMoney = new Money(Currency.getInstance(Locale.UK), new BigDecimal("100.1"));
        Money otherMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.100"));
        boolean pass = false;
        try {
            currentMoney.isLesser(otherMoney);
        } catch (UnsupportedCurrencyException e) {
            pass = true;
        }

        assertEquals(pass, true);
    }

    @Test
    public void TestMoneyLesserEqualDifferentCurrency() {

        Money currentMoney = new Money(Currency.getInstance(Locale.UK), new BigDecimal("10001.1"));
        Money otherMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.100"));
        boolean pass = false;
        try {
            currentMoney.isLesserOrEqual(otherMoney);
        } catch (UnsupportedCurrencyException e) {
            pass = true;
        }

        assertEquals(pass, true);
    }

    @Test
    public void TestMoneyGreater() {

        Money currentMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.1"));
        Money otherMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("1.001"));
        boolean greater = false;
        try {
            greater = currentMoney.isGreater(otherMoney);
        } catch (UnsupportedCurrencyException e) {

        }

        assertEquals(greater, true);
    }

    @Test
    public void TestMoneyGreaterEqual() {

        Money currentMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.1"));
        Money otherMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.100"));
        boolean greater = false;
        try {
            greater = currentMoney.isGreaterOrEqual(otherMoney);
        } catch (UnsupportedCurrencyException e) {

        }

        assertEquals(greater, true);
    }

    @Test
    public void TestMoneyGreaterDifferentCurrency() {

        Money currentMoney = new Money(Currency.getInstance(Locale.UK), new BigDecimal("10001.1"));
        Money otherMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.100"));
        boolean pass = false;
        try {
            currentMoney.isGreater(otherMoney);
        } catch (UnsupportedCurrencyException e) {
            pass = true;
        }

        assertEquals(pass, true);
    }

    @Test
    public void TestMoneyGreaterEqualDifferentCurrency() {

        Money currentMoney = new Money(Currency.getInstance(Locale.UK), new BigDecimal("10001.1"));
        Money otherMoney = new Money(Currency.getInstance(Locale.US), new BigDecimal("10001.100"));
        boolean pass = false;
        try {
            currentMoney.isGreaterOrEqual(otherMoney);
        } catch (UnsupportedCurrencyException e) {
            pass = true;
        }

        assertEquals(pass, true);
    }

}
