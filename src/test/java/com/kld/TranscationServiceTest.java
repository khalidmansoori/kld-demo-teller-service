package com.kld;

import com.kld.demoteller.*;
import com.kld.demoteller.impl.AccountImpl;
import com.kld.demoteller.impl.TransactionServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Currency;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
//import static org.mockito.Mockito.when;

public class TranscationServiceTest {
    protected TransactionService transactionService;


    //  @Mock
    //  TransactionIdGenerator mockTansactionIdGenerator;

    @Before
    public void setup() {

        transactionService = new TransactionServiceImpl(new TransactionIdGenerator() {
            @Override
            public String generate() {

                return UUID.randomUUID().toString();
            }
        });

    }

    @Test
    public void TestTransactionServiceSimple() {

        Money money = new Money(new BigDecimal("100.001"));
        Account fromAccount = new AccountImpl(new AccountNumber(new BigInteger("12345")), new BigDecimal("1000.001"));
        Account toAccount = new AccountImpl(new AccountNumber(new BigInteger("12345")));

        Transaction transaction = transactionService.transfer(money, fromAccount, toAccount, "");

        assertEquals(transaction, transactionService.transactions().get(0));

        assertEquals(transaction.getAmount(), money);
        assertEquals(transaction.creditAccount(), toAccount);
        assertEquals(transaction.debitAccount(), fromAccount);
        assertEquals(transaction.getResult(), TransactionResult.SUCCESS);
        assertEquals(transaction.getState(), TransactionState.COMPLETED);

        ConcurrentHashMap<TransactionState, Long> timestamps = transaction.timestamps();
        assertNotNull(timestamps.get(TransactionState.INITIATED));
        assertNotNull(timestamps.get(TransactionState.CREDITED));
        assertNotNull(timestamps.get(TransactionState.DEBITED));
        assertNotNull(timestamps.get(TransactionState.COMPLETED));

        assertEquals(new Money(new BigDecimal("900.000")), fromAccount.balance());
        assertEquals(new Money(new BigDecimal("100.001")), toAccount.balance());
    }

    @Test
    public void TestTransactionServiceSelfFail() {

        Money money = new Money(new BigDecimal("100.001"));
        Account fromAccount = new AccountImpl(new AccountNumber(new BigInteger("12345")), new BigDecimal("100000.001"));

        Transaction transaction = transactionService.transfer(money, fromAccount, fromAccount, "");
        assertEquals(new Money(new BigDecimal("100000.001")), fromAccount.balance());

        assertEquals(transaction, transactionService.transactions().get(0));

        assertEquals(transaction.getAmount(), money);
        assertEquals(transaction.creditAccount(), fromAccount);
        assertEquals(transaction.debitAccount(), fromAccount);
        assertEquals(transaction.getResult(), TransactionResult.SELF_TRANSFER_FAIL);
        assertEquals(transaction.getState(), TransactionState.DEBIT_FAIL);

        ConcurrentHashMap<TransactionState, Long> timestamps = transaction.timestamps();
        assertNotNull(timestamps.get(TransactionState.INITIATED));
        assertNotNull(timestamps.get(TransactionState.DEBIT_FAIL));
    }

    @Test
    public void TestTransactionServiceDebitFail() {

        Money money = new Money(new BigDecimal("100.001"));
        Account fromAccount = new AccountImpl(new AccountNumber(new BigInteger("12345")), new BigDecimal("10.001"));
        Account toAccount = new AccountImpl(new AccountNumber(new BigInteger("12345")));

        Transaction transaction = transactionService.transfer(money, fromAccount, toAccount, "");
        assertEquals(new Money(new BigDecimal("10.001")), fromAccount.balance());
        assertEquals(new Money(new BigDecimal("0.0")), toAccount.balance());

        assertEquals(transaction, transactionService.transactions().get(0));

        assertEquals(transaction.getAmount(), money);
        assertEquals(transaction.creditAccount(), toAccount);
        assertEquals(transaction.debitAccount(), fromAccount);
        assertEquals(transaction.getResult(), TransactionResult.DEBIT_FAILED_INSUFFICIENT_FUND);
        assertEquals(transaction.getState(), TransactionState.DEBIT_FAIL);

        ConcurrentHashMap<TransactionState, Long> timestamps = transaction.timestamps();
        assertNotNull(timestamps.get(TransactionState.INITIATED));
        assertNotNull(timestamps.get(TransactionState.DEBIT_FAIL));
    }

    @Test
    public void TestTransactionServiceCreditFail() {

        Money money = new Money(new BigDecimal("100.001"));
        Account fromAccount = new AccountImpl(new AccountNumber(new BigInteger("12345")), new BigDecimal("100000.001"));
        TestAccount toAccount = new TestAccount(new AccountNumber(new BigInteger("12345")));
        toAccount.setCurrency(Currency.getInstance(Locale.CANADA));

        Transaction transaction = transactionService.transfer(money, fromAccount, toAccount, "");
        assertEquals(new Money(new BigDecimal("100000.001")), fromAccount.balance());
        assertEquals(new Money(new BigDecimal("0.0")), toAccount.balance());

        assertEquals(transaction, transactionService.transactions().get(0));

        assertEquals(transaction.getAmount(), money);
        assertEquals(transaction.creditAccount(), toAccount);
        assertEquals(transaction.debitAccount(), fromAccount);
        assertEquals(TransactionResult.CREDIT_FAILED_UNSUPPORTED_CURRENCY, transaction.getResult());
        assertEquals(TransactionState.REFUND_SUCCESS, transaction.getState());

        ConcurrentHashMap<TransactionState, Long> timestamps = transaction.timestamps();
        assertNotNull(timestamps.get(TransactionState.INITIATED));
        assertNotNull(timestamps.get(TransactionState.DEBITED));
        assertNotNull(timestamps.get(TransactionState.CREDIT_FAIL));
        assertNotNull(timestamps.get(TransactionState.REFUND_INITIATED));
        assertNotNull(timestamps.get(TransactionState.REFUND_SUCCESS));
    }

    @Test
    public void TestTransactionServiceDebitFailUnsupportedCurrency() {

        Money money = new Money(new BigDecimal("100.001"));
        TestAccount fromAccount = new TestAccount(new AccountNumber(new BigInteger("12345")),
                                                  new BigDecimal("100000.001"));
        fromAccount.setCurrency(Currency.getInstance(Locale.CANADA));
        TestAccount toAccount = new TestAccount(new AccountNumber(new BigInteger("12345")));
        toAccount.setCurrency(Currency.getInstance(Locale.CANADA));

        Transaction transaction = transactionService.transfer(money, fromAccount, toAccount, "");
        assertEquals(new Money(new BigDecimal("100000.001")), fromAccount.balance());
        assertEquals(new Money(new BigDecimal("0.0")), toAccount.balance());

        assertEquals(transaction, transactionService.transactions().get(0));

        assertEquals(transaction.getAmount(), money);
        assertEquals(transaction.creditAccount(), toAccount);
        assertEquals(transaction.debitAccount(), fromAccount);
        assertEquals(TransactionResult.DEBIT_FAILED_UNSUPPORTED_CURRENCY, transaction.getResult());
        assertEquals(TransactionState.DEBIT_FAIL, transaction.getState());

        ConcurrentHashMap<TransactionState, Long> timestamps = transaction.timestamps();
        assertNotNull(timestamps.get(TransactionState.INITIATED));
        assertNotNull(timestamps.get(TransactionState.DEBIT_FAIL));


    }

    @Test
    public void TestTransactionServiceMultiple() {

        Money money = new Money(new BigDecimal("100.001"));
        Account fromAccount = new AccountImpl(new AccountNumber(new BigInteger("12345")), new BigDecimal("1000.001"));
        Account toAccount = new AccountImpl(new AccountNumber(new BigInteger("12345")));

        Transaction transaction = transactionService.transfer(money, fromAccount, toAccount, "");
        assertEquals(new Money(new BigDecimal("900.000")), fromAccount.balance());
        assertEquals(new Money(new BigDecimal("100.001")), toAccount.balance());

        assertEquals(transaction, transactionService.transactions().get(0));

        assertEquals(transaction.getAmount(), money);
        assertEquals(transaction.creditAccount(), toAccount);
        assertEquals(transaction.debitAccount(), fromAccount);
        assertEquals(transaction.getResult(), TransactionResult.SUCCESS);
        assertEquals(transaction.getState(), TransactionState.COMPLETED);

        ConcurrentHashMap<TransactionState, Long> timestamps = transaction.timestamps();
        assertNotNull(timestamps.get(TransactionState.INITIATED));
        assertNotNull(timestamps.get(TransactionState.CREDITED));
        assertNotNull(timestamps.get(TransactionState.DEBITED));
        assertNotNull(timestamps.get(TransactionState.COMPLETED));

        transaction = transactionService.transfer(money, fromAccount, toAccount, "");

        assertEquals(new Money(new BigDecimal("799.999")), fromAccount.balance());
        assertEquals(new Money(new BigDecimal("200.002")), toAccount.balance());

        assertEquals(transaction, transactionService.transactions().get(1));

        assertEquals(transaction.getAmount(), money);
        assertEquals(transaction.creditAccount(), toAccount);
        assertEquals(transaction.debitAccount(), fromAccount);
        assertEquals(transaction.getResult(), TransactionResult.SUCCESS);
        assertEquals(transaction.getState(), TransactionState.COMPLETED);

        timestamps = transaction.timestamps();
        assertNotNull(timestamps.get(TransactionState.INITIATED));
        assertNotNull(timestamps.get(TransactionState.CREDITED));
        assertNotNull(timestamps.get(TransactionState.DEBITED));
        assertNotNull(timestamps.get(TransactionState.COMPLETED));


        transaction = transactionService.transfer(money, fromAccount, toAccount, "");
        assertEquals(new Money(new BigDecimal("699.998")), fromAccount.balance());
        assertEquals(new Money(new BigDecimal("300.003")), toAccount.balance());


        assertEquals(transaction, transactionService.transactions().get(2));

        assertEquals(transaction.getAmount(), money);
        assertEquals(transaction.creditAccount(), toAccount);
        assertEquals(transaction.debitAccount(), fromAccount);
        assertEquals(transaction.getResult(), TransactionResult.SUCCESS);
        assertEquals(transaction.getState(), TransactionState.COMPLETED);

        timestamps = transaction.timestamps();
        assertNotNull(timestamps.get(TransactionState.INITIATED));
        assertNotNull(timestamps.get(TransactionState.CREDITED));
        assertNotNull(timestamps.get(TransactionState.DEBITED));
        assertNotNull(timestamps.get(TransactionState.COMPLETED));

        transaction = transactionService.transfer(money, fromAccount, toAccount, "");
        assertEquals(new Money(new BigDecimal("599.997")), fromAccount.balance());
        assertEquals(new Money(new BigDecimal("400.004")), toAccount.balance());

        assertEquals(transaction, transactionService.transactions().get(3));

        assertEquals(transaction.getAmount(), money);
        assertEquals(transaction.creditAccount(), toAccount);
        assertEquals(transaction.debitAccount(), fromAccount);
        assertEquals(transaction.getResult(), TransactionResult.SUCCESS);
        assertEquals(transaction.getState(), TransactionState.COMPLETED);

        timestamps = transaction.timestamps();
        assertNotNull(timestamps.get(TransactionState.INITIATED));
        assertNotNull(timestamps.get(TransactionState.CREDITED));
        assertNotNull(timestamps.get(TransactionState.DEBITED));
        assertNotNull(timestamps.get(TransactionState.COMPLETED));

        transaction = transactionService.transfer(money, fromAccount, toAccount, "");
        assertEquals(new Money(new BigDecimal("499.996")), fromAccount.balance());
        assertEquals(new Money(new BigDecimal("500.005")), toAccount.balance());

        assertEquals(transaction, transactionService.transactions().get(4));

        assertEquals(transaction.getAmount(), money);
        assertEquals(transaction.creditAccount(), toAccount);
        assertEquals(transaction.debitAccount(), fromAccount);
        assertEquals(transaction.getResult(), TransactionResult.SUCCESS);
        assertEquals(transaction.getState(), TransactionState.COMPLETED);

        timestamps = transaction.timestamps();
        assertNotNull(timestamps.get(TransactionState.INITIATED));
        assertNotNull(timestamps.get(TransactionState.CREDITED));
        assertNotNull(timestamps.get(TransactionState.DEBITED));
        assertNotNull(timestamps.get(TransactionState.COMPLETED));

        Money lastMoney = new Money(new BigDecimal("499.996"));
        transaction = transactionService.transfer(lastMoney, fromAccount, toAccount, " Sending money to friend");
        assertEquals(new Money(new BigDecimal("0.000")), fromAccount.balance());
        assertEquals(new Money(new BigDecimal("1000.001")), toAccount.balance());
        assertEquals(" Sending money to friend", transaction.remark());

        assertEquals(transaction, transactionService.transactions().get(5));

        assertEquals(transaction.getAmount(), lastMoney);
        assertEquals(transaction.creditAccount(), toAccount);
        assertEquals(transaction.debitAccount(), fromAccount);
        assertEquals(transaction.getResult(), TransactionResult.SUCCESS);
        assertEquals(transaction.getState(), TransactionState.COMPLETED);

        timestamps = transaction.timestamps();
        assertNotNull(timestamps.get(TransactionState.INITIATED));
        assertNotNull(timestamps.get(TransactionState.CREDITED));
        assertNotNull(timestamps.get(TransactionState.DEBITED));
        assertNotNull(timestamps.get(TransactionState.COMPLETED));


        Money returnMoney = new Money(new BigDecimal("1000.001"));
        transaction = transactionService.transfer(returnMoney, toAccount, fromAccount, "Returning all Money Back");
        assertEquals(new Money(new BigDecimal("1000.001")), fromAccount.balance());
        assertEquals(new Money(new BigDecimal("0.000")), toAccount.balance());

        assertEquals(transaction, transactionService.transactions().get(6));

        assertEquals("Returning all Money Back", transaction.remark());

        assertEquals(transaction.getAmount(), returnMoney);
        assertEquals(transaction.creditAccount(), fromAccount);
        assertEquals(transaction.debitAccount(), toAccount);
        assertEquals(transaction.getResult(), TransactionResult.SUCCESS);
        assertEquals(transaction.getState(), TransactionState.COMPLETED);

        timestamps = transaction.timestamps();
        assertNotNull(timestamps.get(TransactionState.INITIATED));
        assertNotNull(timestamps.get(TransactionState.CREDITED));
        assertNotNull(timestamps.get(TransactionState.DEBITED));
        assertNotNull(timestamps.get(TransactionState.COMPLETED));
    }

}
