package com.kld.exception;

import java.util.Currency;

public class UnsupportedCurrencyException extends Exception {
    private Currency unsupportedCurrency;

    public UnsupportedCurrencyException(Currency currency) {

        this.unsupportedCurrency = currency;
    }

    public Currency getUnsupportedCurrency() {

        return unsupportedCurrency;
    }

}
