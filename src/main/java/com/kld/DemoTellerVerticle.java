package com.kld;

import com.google.gson.Gson;
import com.kld.demoteller.*;
import com.kld.demoteller.impl.AccountServiceImpl;
import com.kld.demoteller.impl.CustomerServiceImpl;
import com.kld.demoteller.impl.TellerServiceImpl;
import com.kld.demoteller.impl.TransactionServiceImpl;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Properties;
import java.util.Random;

//import org.aspectj.lang.annotation.After;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;

/**
 * Entry Verticle.
 */
public class DemoTellerVerticle extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(DemoTellerVerticle.class);
    private static final String CUSTOMER_NAME = "cust_name";
    private static final String ACCOUNT_NUMBER = "account_number";
    private static final String TO_ACCOUNT_NUMBER = "to_account_number";
    private static final String AMOUNT = "amount";
    private static  int PORT = 8080;

    //TO-DO Work on generators
    protected CustomerService customerService = new CustomerServiceImpl(new CustomerNumberGenerator() {
        @Override
        public CustomerNumber generate() {

            return new CustomerNumber(randomBigInt());
        }
    });
    protected AccountService accountService = new AccountServiceImpl(customerService, new AccountNumberGenerator() {
        @Override
        public AccountNumber generate() {

            return new AccountNumber(randomBigInt());
        }
    });
    protected TransactionService transactionService = new TransactionServiceImpl(new TransactionIdGenerator() {
        @Override
        public String generate() {

            Random randomGenerator = new Random();
            Integer number = randomGenerator.nextInt(999999999);
            return number.toString();
        }
    });


    protected TellerService tellerService = new TellerServiceImpl(accountService, transactionService);
    private HttpServer server;

    private BigInteger randomBigInt() {

        Random randomGenerator = new Random();
        Integer number = randomGenerator.nextInt(999999999);
        return new BigInteger(number.toString());
    }

    @Override
    public void start(Future<Void> startFuture) {

        String userPort = System.getProperties().getProperty("com.kld.demoteller.port");
        if (userPort != null && !userPort.isEmpty()) {
            log.info("User port from properties " + userPort);
            try {
                int port = Integer.parseInt(userPort);
                PORT = port;
            }
            catch (NumberFormatException e) {
                log.warn("Failed to get port from " + e.getLocalizedMessage() + " default " + PORT + " will be used.");
            }
        }

        server = vertx.createHttpServer();
        Router router = buildRouter();
        server.requestHandler(router::accept).listen(PORT, asyncResult -> {
            if (asyncResult.succeeded()) {
                startFuture.complete();
                log.info("Teller Service is available on port " + PORT);
            } else {
                startFuture.fail(asyncResult.cause());
                log.error("Unable to start Service on port " + PORT);
            }
        });
    }

    @Override
    public void stop(Future<Void> stopFuture) {

        server.close(v -> {
            if (v.succeeded()) {
                stopFuture.complete();
            } else {
                stopFuture.fail(v.cause());
                log.error("Unable to stop HTTP server");
            }
        });
    }

    private Router buildRouter() {

        Router router = Router.router(vertx);
        router.route().handler(this::addResponseHeader);
        router.put("/create-account/:" + CUSTOMER_NAME).handler(this::createAccount);
        router.get("/balance/:" + ACCOUNT_NUMBER).handler(this::balance);
        router.post("/withdraw/:" + AMOUNT + "/:" + ACCOUNT_NUMBER).handler(this::withdraw);
        router.post("/deposit/:" + AMOUNT + "/:" + ACCOUNT_NUMBER).handler(this::deposit);
        router.post("/transfer/:" + AMOUNT + "/:" + ACCOUNT_NUMBER + "/:" + TO_ACCOUNT_NUMBER).handler(this::transfer);

        return router;
    }

    private void createAccount(RoutingContext routingContext) {

        String customerName = routingContext.request().getParam(CUSTOMER_NAME);
        if (customerName == null || customerName.isEmpty()) {
            routingContext.response().setStatusCode(400)
                          .end(Responses.message("Please provide valid " + CUSTOMER_NAME).toBuffer());
            return;
        }

        vertx.executeBlocking(future -> {
            // Calling balance asynchronously
            log.info("Asynchronously creating account for " + customerName);
            AccountNumber accountNumber = tellerService
                    .createAccount(new CustomerInfo(customerName), "source: REST API");
            future.complete(accountNumber);
        }, res -> {
            AccountNumber accountNumber = (AccountNumber) res.result();
            log.info("Account " + accountNumber.getAccountNumber() + ", assigned to " + customerName);
            Gson gson = new Gson();
            String json = gson.toJson(accountNumber);
            routingContext.response().end(json);
        });

    }


    private void balance(RoutingContext routingContext) {

        String accountNumber = routingContext.request().getParam(ACCOUNT_NUMBER);
        if (accountNumber == null || accountNumber.isEmpty()) {
            routingContext.response().setStatusCode(400)
                          .end(Responses.message("Please provide valid " + ACCOUNT_NUMBER).toBuffer());
            return;
        }

        vertx.executeBlocking(future -> {
            // Calling balance asynchronously
            log.info("Asynchronously getting balance for account " + accountNumber);
            BigDecimal balance = tellerService.balance(new BigInteger(accountNumber));
            future.complete(balance);
        }, res -> {
            // This would be executed back on event loop after call is completed.
            BigDecimal balance = (BigDecimal) res.result();
            if (balance == null) {
                String message = "Account " + accountNumber + " does not exist";
                log.warn(message);
                routingContext.response().setStatusCode(404).end(Responses.message(message).toBuffer());
                return;
            }

            log.info("Account " + accountNumber + ", balance " + balance);
            Money money = new Money(balance);
            Gson gson = new Gson();
            String json = gson.toJson(money);
            routingContext.response().end(json);
        });
    }

    private void withdraw(RoutingContext routingContext) {

        String amount;
        String accountNumber;
        try {
            amount = extractParam(AMOUNT, routingContext);
            accountNumber = extractParam(ACCOUNT_NUMBER, routingContext);
        } catch (IllegalArgumentException e) {
            routingContext.response().setStatusCode(400)
                          .end(Responses.message("Please provide valid " + e.getMessage()).toBuffer());
            return;
        }

        vertx.executeBlocking(future -> {
            // Call some blocking API that takes a significant amount of time to return
            log.info("Asynchronously withdrawing amount " + amount + " from account " + accountNumber);
            TransactionResult result = tellerService
                    .withdraw(new BigInteger(accountNumber), new BigDecimal(amount), "source REST API");
            future.complete(result);
        }, res -> {
            TransactionResult result = (TransactionResult) res.result();
            log.info("Withdraw transaction result " + result + " for amount " + amount + " from account " + accountNumber);
            handleTransactionResult(result, routingContext);
        });
    }

    private void deposit(RoutingContext routingContext) {

        String amount;
        String accountNumber;
        try {
            amount = extractParam(AMOUNT, routingContext);
            accountNumber = extractParam(ACCOUNT_NUMBER, routingContext);
        } catch (IllegalArgumentException e) {
            routingContext.response().setStatusCode(400)
                          .end(Responses.message("Please provide valid " + e.getMessage()).toBuffer());
            return;
        }

        vertx.executeBlocking(future -> {
            // Call some blocking API that takes a significant amount of time to return
            log.info("Asynchronously depositing amount " + amount + " to account " + accountNumber);
            TransactionResult result = tellerService
                    .deposit(new BigInteger(accountNumber), new BigDecimal(amount), "source REST API");
            future.complete(result);
        }, res -> {
            TransactionResult result = (TransactionResult) res.result();
            log.info("Deposit transaction result " + result + " for amount " + amount + " to account " + accountNumber);
            handleTransactionResult(result, routingContext);
        });
    }


    private void transfer(RoutingContext routingContext) {

        String amount;
        String accountNumber;
        String toAccountNumber;
        try {
            amount = extractParam(AMOUNT, routingContext);
            accountNumber = extractParam(ACCOUNT_NUMBER, routingContext);
            toAccountNumber = extractParam(TO_ACCOUNT_NUMBER, routingContext);
        } catch (IllegalArgumentException e) {
            routingContext.response().setStatusCode(400)
                          .end(Responses.message("Please provide valid " + e.getMessage()).toBuffer());
            return;
        }

        vertx.executeBlocking(future -> {
            // Call some blocking API that takes a significant amount of time to return
            log.info("Asynchronously transferring amount " + amount + " from account " + accountNumber + " to account" +
                     " " +
                     toAccountNumber);
            TransactionResult result = tellerService
                    .transferMoney(new BigInteger(accountNumber), new BigInteger(toAccountNumber),
                                   new BigDecimal(amount), "source REST API");
            future.complete(result);
        }, res -> {
            TransactionResult result = (TransactionResult) res.result();
            log.info("Transfer transaction result " + result + " for amount " + amount + " from account " + accountNumber +
                     " to " + " account " + toAccountNumber);
            handleTransactionResult(result, routingContext);
        });

    }

    private void addResponseHeader(RoutingContext ctx) {

        ctx.response().putHeader("Content-Type", "application/json");
        ctx.next();
    }


    private String extractParam(String param, RoutingContext routingContext) throws IllegalArgumentException {

        String argument = routingContext.request().getParam(param);
        if (argument == null || argument.isEmpty()) {
            routingContext.response().setStatusCode(400)
                          .end(Responses.message("Please provide valid " + param).toBuffer());
            throw new IllegalArgumentException(param);
        }

        return argument;
    }

    private void handleTransactionResult(TransactionResult result, RoutingContext routingContext) {

        if (result == TransactionResult.SUCCESS) {
            routingContext.response().setStatusCode(200).end(Responses.success().toBuffer());
            return;
        }

        routingContext.response().setStatusCode(400)
                      .end(Responses.message("Transaction failed " + result).toBuffer());
    }
}
