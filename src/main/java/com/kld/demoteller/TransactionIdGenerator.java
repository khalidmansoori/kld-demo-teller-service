package com.kld.demoteller;

public interface TransactionIdGenerator {
    String generate();
}
