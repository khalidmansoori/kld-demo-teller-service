package com.kld.demoteller;

public enum AccountStatus {
    ACTIVE, TEMPORARLY_SUSPENDED, SUSPENDED, DELETED
}
