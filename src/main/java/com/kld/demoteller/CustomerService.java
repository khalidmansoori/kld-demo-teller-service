package com.kld.demoteller;

public interface CustomerService {
    Customer createORGetCustomer(CustomerInfo customerInfo);
}
