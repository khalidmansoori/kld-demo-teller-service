package com.kld.demoteller;

public class CustomerInfo {
    /* Name of customer string for demo app rather than a class */ String name;

    public CustomerInfo(String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

}
