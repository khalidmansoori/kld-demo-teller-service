package com.kld.demoteller;

import java.util.Currency;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Represent an account.
 */
public interface Account {

    /**
     * @return a account id
     */
    AccountNumber accountNumber();

    /**
     * @return the currency of an account;
     */
    Currency currency();

    /**
     * @return type of account
     */
    AccountType getType();

    /**
     * assumed to be of same currency as of account.
     *
     * @return current amount of balance on the account
     */
    Money balance();

    /**
     * @return transactions related to the account
     */
    CopyOnWriteArrayList<TransactionRecord> transactions();

    TransactionResult credit(Money money);

    TransactionResult debit(Money money);

    //TO-DO future.
    //boolean isBlocked();
}
