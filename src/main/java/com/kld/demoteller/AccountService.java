package com.kld.demoteller;

import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;

public interface AccountService {

    /**
     * Creates account based on information received from customer and initial balance;
     *
     * @return just created {@link Account} with given accountInfo.
     */
    Account createAccount(BigDecimal amount, CustomerInfo customerInfo);


    /**
     * @param accountNumber id of account we are looking for
     * @return {@code null} if no account were found, or an instance of {@link Account} with given id
     */
    @Nullable Account accountByNumber(AccountNumber accountNumber);

    //  /**
    //   * deletes account based on information received from customer.
    //   */
    //  Account deleteAccount(Customer accountInfo);

    /* TO-DO add searching of accounts based on customer information. */


}
