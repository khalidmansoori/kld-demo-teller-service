package com.kld.demoteller;

public interface CustomerNumberGenerator {
    CustomerNumber generate();
}
