package com.kld.demoteller.impl;

import com.kld.demoteller.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CopyOnWriteArrayList;

public class TransactionServiceImpl implements TransactionService {

    private static final Logger log = LoggerFactory.getLogger(TransactionServiceImpl.class);

    protected TransactionIdGenerator transactionIdGenerator;

    protected CopyOnWriteArrayList<Transaction> transactions;

    public TransactionServiceImpl(TransactionIdGenerator transactionIdGenerator) {

        this.transactionIdGenerator = transactionIdGenerator;
        this.transactions = new CopyOnWriteArrayList<>();
    }

    /**
     * @param money
     * @param debitAccount
     * @param creditAccount
     * @param remark
     * @return Transaction details of transfer;
     */
    @Override
    public Transaction transfer(Money money, Account debitAccount, Account creditAccount, String remark) {

        String transactionId = transactionIdGenerator.generate();

        Transaction transaction = new TransactionImpl(transactionId, debitAccount, creditAccount, money,
                                                      TransactionState.INITIATED, TransactionResult.SUCCESS, remark);

        log.warn("Started " + buildTransferMessage(transaction, money, debitAccount, creditAccount, remark));

        if (creditAccount.accountNumber() == debitAccount.accountNumber()) {
            transaction.setResult(TransactionResult.SELF_TRANSFER_FAIL);
            transaction.setState(TransactionState.DEBIT_FAIL);
            log.warn("Failed " + buildTransferMessage(transaction, money, debitAccount, creditAccount, remark));
            transactions.add(transaction);
            return transaction;
        }

        TransactionResult debitResult = debitAccount.debit(money);

        if (debitResult != TransactionResult.SUCCESS) {
            transaction.setResult(debitResult);
            transaction.setState(TransactionState.DEBIT_FAIL);

            log.warn("Failed " + buildTransferMessage(transaction, money, debitAccount, creditAccount, remark));
            transactions.add(transaction);
            return transaction;
        }

        transaction.setState(TransactionState.DEBITED);

        TransactionResult creditResult = creditAccount.credit(money);
        transaction.setResult(creditResult);

        if (creditResult != TransactionResult.SUCCESS) {
            transaction.setState(TransactionState.CREDIT_FAIL);
            log.warn("Failed " + buildTransferMessage(transaction, money, debitAccount, creditAccount, remark));

            // TO-DO ADDITIONAL Processing and Alerts
            transaction.setState(TransactionState.REFUND_INITIATED);
            log.info("Refund initiated " +
                     buildTransferMessage(transaction, money, debitAccount, creditAccount, remark));
            TransactionResult refundResult = debitAccount.credit(money);

            if (refundResult != TransactionResult.SUCCESS) {
                transaction.setState(TransactionState.REFUND_FAILED);
                //WARNING TO-DO create alert for manuel intervention.

                log.error("Refund failed " +
                          buildTransferMessage(transaction, money, debitAccount, creditAccount, remark));
                transactions.add(transaction);
                return transaction;
            }

            transaction.setState(TransactionState.REFUND_SUCCESS);

            log.info("Refund success " + buildTransferMessage(transaction, money, debitAccount, creditAccount, remark));
            transactions.add(transaction);
            return transaction;
        }

        transaction.setState(TransactionState.CREDITED);
        // DO NOT assume the timing, set state can do many things, may be over network or slow i/o.
        transaction.setState(TransactionState.COMPLETED);

        log.info("Success " + buildTransferMessage(transaction, money, debitAccount, creditAccount, remark));
        transactions.add(transaction);
        return transaction;
    }

    /**
     * @return all transactions;
     */
    @Override
    public CopyOnWriteArrayList<Transaction> transactions() {

        return transactions;
    }

    private String buildTransferMessage(
            Transaction transaction, Money money, Account debitAccount, Account creditAccount, String remark) {

        return new StringBuilder().append("Transaction: ").append(transaction.toString()).append(", debit Account:")
                                  .append(debitAccount).append(", creditAccount:").append(creditAccount)
                                  .append(", money:").append(money).append(", remark:").append(remark).toString();
    }

}
