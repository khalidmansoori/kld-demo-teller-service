package com.kld.demoteller.impl;

import com.kld.demoteller.*;
import com.kld.exception.UnsupportedCurrencyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class AccountImpl implements Account {

    public static final long LOCK_WAIT_INTERVAL = 100L; // in milliseconds
    protected static final Logger log = LoggerFactory.getLogger(AccountImpl.class);
    protected final AccountNumber accountNumber;
    protected final AccountType type;
    private final transient Lock lock;
    protected BigDecimal balance;
    protected Currency currency;
    CopyOnWriteArrayList<TransactionRecord> transactions;

    public AccountImpl(AccountNumber accountNumber) {

        this(accountNumber, new BigDecimal("0.0"));
    }


    public AccountImpl(AccountNumber accountNumber, BigDecimal initialBalance) {

        this.accountNumber = accountNumber;
        //TO-DO support many currencies via arguments and accept default from System properties.
        this.currency = Currency.getInstance(Locale.UK);
        this.balance = initialBalance;
        //TO-DO Support many types;
        this.type = AccountType.SAVING;
        this.transactions = new CopyOnWriteArrayList<>();
        this.lock = new ReentrantLock();
    }

    @Override
    public AccountNumber accountNumber() {

        return this.accountNumber;
    }

    @Override
    public Currency currency() {

        try {
            lock.lock();
            return this.currency;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public AccountType getType() {

        return this.type;
    }

    @Override
    public Money balance() {

        try {
            lock.lock();
            return new Money(this.balance);
        } finally {
            lock.unlock();
        }
    }

    /**
     * @return transactions related to the account
     */
    @Override
    public CopyOnWriteArrayList<TransactionRecord> transactions() {

        return transactions;
    }

    @Override
    public TransactionResult credit(Money creditMoney) {

        Objects.requireNonNull(creditMoney, "creditMoney cannot be null");
        //TO-DO Add Validator

        //TO-DO use generator
        String transactionId = UUID.randomUUID().toString();
        TransactionResult result = TransactionResult.SUCCESS;

        try {
            if (lock.tryLock(LOCK_WAIT_INTERVAL, TimeUnit.MILLISECONDS)) {
                try {
                    Money currentMoney = new Money(this.currency, this.balance);
                    log.info(toString() + ": Credit requested " + creditMoney);

                    try {
                        if (creditMoney.isLesserOrEqual(new Money(new BigDecimal("0.0")))) {
                            result = TransactionResult.CREDIT_FAILED_INVALID_AMOUNT;
                            handleCreditFailure(creditMoney, transactionId, result);
                            return result;
                        }

                        currentMoney.add(creditMoney);
                    } catch (UnsupportedCurrencyException e) {
                        result = TransactionResult.CREDIT_FAILED_UNSUPPORTED_CURRENCY;

                        handleCreditFailure(creditMoney, transactionId, result);
                    }

                    this.balance = currentMoney.getAmount();
                    handleCreditSuccess(creditMoney, transactionId);

                    return result;

                } finally {
                    lock.unlock();
                }
            }
        } catch (InterruptedException e) {

            log.error(e.getLocalizedMessage(), e);
        }

        result = TransactionResult.INTERNAL_SYSTEM_ERROR;
        handleCreditFailure(creditMoney, transactionId, result);
        return result;
    }

    @Override
    public TransactionResult debit(Money debitMoney) {

        Objects.requireNonNull(debitMoney, "Debit money cannot be null");

        //if(AccountService.isBlocked(this)); TO-DO Future.

        // Assuming negative balance is NOT allowed, considering account type SAVING
        // However it may be allowed for account type LOAN, But not considered now.
        // TO-DO Add Validators.
        TransactionResult result = TransactionResult.SUCCESS;
        //TO-DO Use generator
        String transactionId = UUID.randomUUID().toString();

        try {
            if (lock.tryLock(LOCK_WAIT_INTERVAL, TimeUnit.MILLISECONDS)) {
                try {
                    Money currentMoney = new Money(this.currency, this.balance);
                    log.info(toString() + ": Debit requested " + debitMoney);


                    try {
                        if (debitMoney.isLesserOrEqual(new Money(new BigDecimal("0.0")))) {

                            result = TransactionResult.DEBIT_FAILED_INVALID_AMOUNT;
                            handleDebitFailure(debitMoney, transactionId, result);
                            return result;
                        }

                        if (currentMoney.isLesser(debitMoney)) {
                            result = TransactionResult.DEBIT_FAILED_INSUFFICIENT_FUND;
                            handleDebitFailure(debitMoney, transactionId, result);
                            return result;
                        }
                        // TO-DO code would be better if we use validators.
                        currentMoney.subtract(debitMoney);

                    } catch (UnsupportedCurrencyException e) {
                        result = TransactionResult.DEBIT_FAILED_UNSUPPORTED_CURRENCY;
                        handleDebitFailure(debitMoney, transactionId, result);
                    }

                    this.balance = currentMoney.getAmount();
                    handleDebitSuccess(debitMoney, transactionId);
                    return result;

                } finally {
                    lock.unlock();
                }
            }
        } catch (InterruptedException e) {
            log.error(e.getLocalizedMessage(), e);
        }

        result = TransactionResult.INTERNAL_SYSTEM_ERROR;
        handleDebitFailure(debitMoney, transactionId, result);
        return result;
    }

    @Override
    public int hashCode() {

        return Objects.hash(lock, accountNumber, type, balance, currency);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof AccountImpl))
            return false;
        AccountImpl account = (AccountImpl) o;
        return accountNumber.equals(account.accountNumber) && type == account.type && balance.equals(account.balance) &&
               currency.equals(account.currency);
    }

    @Override
    public String toString() {

        return "AccountImpl{" + " accountNumber=" + accountNumber.getAccountNumber().toString() + ", type=" + type +
               ", balance=" + balance() + ", currency=" + currency + '}';
    }

    private void handleCreditFailure(Money creditMoney, String transactionId, TransactionResult result) {

        handleFailure(true, creditMoney, transactionId, result);
    }

    private void handleCreditSuccess(Money creditMoney, String transactionId) {

        handleSuccess(true, creditMoney, transactionId);
    }

    private void handleDebitFailure(Money debitMoney, String transactionId, TransactionResult result) {

        handleFailure(false, debitMoney, transactionId, result);
    }

    private void handleDebitSuccess(Money debitMoney, String transactionId) {

        handleSuccess(false, debitMoney, transactionId);
    }

    private void handleFailure(boolean isCredit, Money money, String transactionId, TransactionResult result) {
        // TO-DO Needs a formatter class for creating descriptions
        String description = (isCredit ? " Credit " : " Debit ") + " failed amount " + money + " via reference " +
                             transactionId + ", failure: " + result;
        transactions
                .add(new TransactionRecordImpl(transactionId, description, money.getAmount(), isCredit, balance, ""));
        log.warn(toString() + description);
    }

    private void handleSuccess(boolean isCredit, Money money, String transactionId) {
        // TO-DO Needs a formatter class for creating descriptions
        String description = (isCredit ? " Credited " : " Debited ") + " amount " + money + " via reference " +
                             transactionId;
        transactions
                .add(new TransactionRecordImpl(transactionId, description, money.getAmount(), isCredit, balance, ""));
        log.info(toString() + description);
    }
}
