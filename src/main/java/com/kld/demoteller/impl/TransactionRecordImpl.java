package com.kld.demoteller.impl;

import com.kld.demoteller.TransactionRecord;

import java.math.BigDecimal;

public class TransactionRecordImpl implements TransactionRecord {

    private final String transactionId;
    private final String description;
    private final BigDecimal amount;
    private final boolean credit;
    private final BigDecimal balance;
    private final Long timestamp;
    private final String remark;


    public TransactionRecordImpl(
            String transactionId, String description, BigDecimal amount, boolean credit, BigDecimal balance) {

        this(transactionId, description, amount, credit, balance, "");
    }

    public TransactionRecordImpl(
            String transactionId, String description, BigDecimal amount, boolean credit, BigDecimal balance,
            String remark) {

        this.description = description;
        this.transactionId = transactionId;
        this.amount = amount;
        this.credit = credit;
        this.balance = balance;
        this.timestamp = System.currentTimeMillis();
        this.remark = remark;
    }

    public static TransactionRecordImpl creditTransactionRecord(
            String transactionId, String description, BigDecimal amount, BigDecimal balance, String remark) {

        return new TransactionRecordImpl(transactionId, description, amount, true, balance, remark);
    }

    public static TransactionRecordImpl debitTransactionRecord(
            String transactionId, String description, BigDecimal amount, BigDecimal balance, String remark) {

        return new TransactionRecordImpl(transactionId, description, amount, false, balance, remark);
    }

    /**
     * @return Id of the transactions;
     */
    @Override
    public String transactionRecordId() {

        return transactionId;
    }

    /**
     * @return when the transaction happen
     */
    @Override
    public Long timestamp() {

        return this.timestamp;
    }

    /**
     * @return when Details of the transactions;
     */
    @Override
    public String description() {

        return this.description;
    }

    /**
     * @return true if money credited else false.
     */
    @Override
    public boolean isCredit() {

        return this.credit;
    }

    /**
     * @return amount of Money debited or credited.
     */
    @Override
    public BigDecimal amount() {

        return this.amount;
    }

    /**
     * @return Balance amount after the transaction;
     */
    @Override
    public BigDecimal balance() {

        return this.balance;
    }

    @Override
    public String remark() {

        return this.remark;
    }
}
