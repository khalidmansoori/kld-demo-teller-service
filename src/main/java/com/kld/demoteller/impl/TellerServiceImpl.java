package com.kld.demoteller.impl;

import com.kld.demoteller.*;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.CopyOnWriteArrayList;

public class TellerServiceImpl implements TellerService {

    private static final Logger log = LoggerFactory.getLogger(TellerServiceImpl.class);

    protected AccountService accountService;
    protected TransactionService transactionService;

    public TellerServiceImpl(AccountService accountService, TransactionService transactionService) {

        this.accountService = accountService;
        this.transactionService = transactionService;
    }

    /**
     * @param customerInfo
     * @param remark
     * @return AccountNumber
     */
    @Override
    public AccountNumber createAccount(CustomerInfo customerInfo, String remark) {

        BigDecimal initialAmount = new BigDecimal("0.0");
        AccountNumber accountNumber = accountService.createAccount(initialAmount, customerInfo).accountNumber();
        return accountNumber;
    }

    /**
     * deposit money to account. assuming all of same currency.
     *
     * @param toAccountNumber
     * @param amount
     * @param remark
     * @return TransactionResult
     */
    @Override
    public TransactionResult deposit(BigInteger toAccountNumber, BigDecimal amount, String remark) {

        TransactionResult depositResult = TransactionResult.SUCCESS;

        StringBuilder depositMessage = new StringBuilder().append(" toAccountNumber ").append(toAccountNumber)
                                                          .append(", amount ").append(amount).append(", remark ")
                                                          .append(remark);
        log.info("Deposit requested " + depositMessage.toString());

        Account fromAccount = accountService.accountByNumber(new AccountNumber(toAccountNumber));
        if (fromAccount == null) {
            depositResult = TransactionResult.ACCOUNT_NOT_FOUND;
            log.error("Deposit failed: " + depositResult + depositMessage.toString());
            return depositResult;
        }

        depositResult = fromAccount.credit(new Money(amount));
        if (depositResult == TransactionResult.SUCCESS) {
            log.info("Deposit Success " + depositMessage.toString());
        } else {
            log.error("Deposit Fail " + depositResult + depositMessage.toString());
        }
        return depositResult;
    }

    /**
     * Withdraw money from the given account. assuming all of same currency.
     *
     * @param fromAccountNumber
     * @param amount
     * @param remark
     * @return TransactionResult
     */
    @Override
    public TransactionResult withdraw(BigInteger fromAccountNumber, BigDecimal amount, String remark) {

        TransactionResult withdrawResult = TransactionResult.SUCCESS;

        StringBuilder withdrawalMessage = new StringBuilder().append(" fromAccountNumber ").append(fromAccountNumber)
                                                             .append(", amount ").append(amount).append(", remark ")
                                                             .append(remark);
        log.info("Withdrawal requested " + withdrawalMessage.toString());

        Account fromAccount = accountService.accountByNumber(new AccountNumber(fromAccountNumber));
        if (fromAccount == null) {

            withdrawResult = TransactionResult.ACCOUNT_NOT_FOUND;
            log.error("Withdrawal failed: " + withdrawResult + withdrawalMessage.toString());
            return withdrawResult;
        }

        withdrawResult = fromAccount.debit(new Money(amount));
        if (TransactionResult.SUCCESS == withdrawResult) {
            log.info("Withdrawal Success " + withdrawalMessage.toString());
        } else {
            log.error("Withdrawal Failed " + withdrawResult + withdrawalMessage.toString());
        }
        return withdrawResult;
    }

    /**
     * Transfers money from the given account to creditor account. assuming all of same currency.
     *
     * @param fromAccountNumber
     * @param toAccountNumber
     * @param amount
     * @param remark
     * @return TransactionResult
     */
    @Override
    public TransactionResult transferMoney(
            BigInteger fromAccountNumber, BigInteger toAccountNumber, BigDecimal amount, String remark) {

        TransactionResult transferResult = TransactionResult.SUCCESS;

        log.info("TransferMoney requested" + transferMoneyMessage(fromAccountNumber, toAccountNumber, amount, remark));

        Account fromAccount = accountService.accountByNumber(new AccountNumber(fromAccountNumber));
        if (fromAccount == null) {

            transferResult = TransactionResult.DEBITOR_ACCOUNT_NOT_FOUND;
            log.error("TransferMoney failed:" +
                      transferMoneyMessage(transferResult, fromAccountNumber, toAccountNumber, amount, remark));
            return transferResult;
        }

        Account toAccount = accountService.accountByNumber(new AccountNumber(toAccountNumber));
        if (toAccount == null) {

            transferResult = TransactionResult.CREDITOR_ACCOUNT_NOT_FOUND;
            log.error("TransferMoney failed:" +
                      transferMoneyMessage(transferResult, fromAccountNumber, toAccountNumber, amount, remark));
            return transferResult;
        }

        Money money = new Money(amount);

        Transaction transaction = transactionService.transfer(money, fromAccount, toAccount, remark);
        transferResult = transaction.getResult();
        if (transferResult == TransactionResult.SUCCESS) {
            log.info("TransferMoney Success:" +
                     transferMoneyMessage(fromAccountNumber, toAccountNumber, amount, remark));
        } else {
            log.error("TransferMoney failed:" +
                      transferMoneyMessage(transferResult, fromAccountNumber, toAccountNumber, amount, remark));
        }
        return transaction.getResult();
    }

    /**
     * Return amount present in the account otherwise null if account not found.
     *
     * @param accountNumber
     * @return amount in the account else null.
     */
    @Nullable
    @Override
    public BigDecimal balance(BigInteger accountNumber) {

        log.info("Balance requested accountNumber :" + accountNumber.toString());

        Account account = accountService.accountByNumber(new AccountNumber(accountNumber));
        if (account == null) {

            log.error("Balance failed: account :" + accountNumber + " not found");
            return null;
        }

        BigDecimal amount = account.balance().getAmount();
        log.error("Balance success: account :" + account.toString());
        return amount;
    }

    @Override
    public CopyOnWriteArrayList<TransactionRecord> transactions(BigInteger fromAccount) {

        return null;
    }

    private String transferMoneyMessage(
            TransactionResult TransactionResult, BigInteger fromAccountNumber, BigInteger toAccountNumber,
            BigDecimal amount, String remark) {

        String transferMoneyMessage = "result:" + TransactionResult +
                                      transferMoneyMessage(fromAccountNumber, toAccountNumber, amount, remark);
        return transferMoneyMessage;
    }

    private String transferMoneyMessage(
            BigInteger fromAccountNumber, BigInteger toAccountNumber, BigDecimal amount, String remark) {

        String transferMoneyMessage = " fromAccountNumber [" + fromAccountNumber + "], toAccountNumber [" +
                                      toAccountNumber + "], amount[" + amount + "], remark [" + remark + "]";
        return transferMoneyMessage;
    }
}
