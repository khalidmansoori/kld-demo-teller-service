package com.kld.demoteller.impl;

import com.kld.demoteller.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;


public class TransactionImpl implements Transaction {

    private static final Logger log = LoggerFactory.getLogger(TransactionImpl.class);

    private final String transactionId;
    private final Account debitAccount;
    private final Account creditAccount;
    private final Money amount;
    private ConcurrentHashMap<TransactionState, Long> timestamps;
    private TransactionState state;
    private TransactionResult result;
    private String remark;

    public TransactionImpl(
            String transactionId, Account debitAccount, Account creditAccount, Money amount, TransactionState state,
            TransactionResult result, String remark) {

        this.transactionId = transactionId;
        this.debitAccount = debitAccount;
        this.creditAccount = creditAccount;
        this.amount = amount;
        this.state = state;
        this.timestamps = new ConcurrentHashMap<>();
        this.timestamps.put(state, System.currentTimeMillis());
        this.result = result;
        this.remark = remark;
    }

    /**
     * @return id of the transaction
     */
    @Override
    public String transactionId() {

        return transactionId;
    }

    /**
     * @return account for which money deducted, Can be Bank itself.
     */
    @Override
    public Account debitAccount() {

        return debitAccount;
    }

    /**
     * @return account from which balance came , Can be Bank itself.
     */
    @Override
    public Account creditAccount() {

        return creditAccount;
    }

    /**
     * @return amount of balance, related to the transaction
     */
    @Override
    public Money getAmount() {

        return amount;
    }

    /**
     * @return when timestamps as transcation moves states.
     */
    @Override
    public ConcurrentHashMap<TransactionState, Long> timestamps() {

        return timestamps;
    }

    /**
     * @return details about the transaction for book keeping.
     */
    @Override
    public String remark() {

        return remark;
    }

    /**
     * @return state of the transaction
     */
    @Override
    public TransactionState getState() {

        return state;
    }

    @Override
    public void setState(TransactionState state) {

        this.state = state;
        this.timestamps.put(state, System.currentTimeMillis());
        log.info("Transaction state changed:" + toString());
    }

    /**
     * @return result of the transaction
     */
    @Override
    public TransactionResult getResult() {

        return result;
    }

    /**
     * keeps the latest transaction result;
     *
     * @param result
     */
    @Override
    public void setResult(TransactionResult result) {

        this.result = result;
        log.info("Transaction result changed:" + toString());
    }

    @Override
    public int hashCode() {

        return Objects.hash(transactionId, debitAccount, creditAccount, amount, timestamps, state, result, remark);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof TransactionImpl))
            return false;
        TransactionImpl that = (TransactionImpl) o;
        return transactionId.equals(that.transactionId) && debitAccount.equals(that.debitAccount) &&
               creditAccount.equals(that.creditAccount) && amount.equals(that.amount) &&
               timestamps.equals(that.timestamps) && state == that.state && result == that.result &&
               remark.equals(that.remark);
    }

    @Override
    public String toString() {

        return "Transaction{" + "transactionId='" + transactionId + '\'' + ", debitAccount=" + debitAccount +
               ", creditAccount=" + creditAccount + ", amount=" + amount + ", timestamp=" + timestamps + ", state=" +
               state + ", result=" + result + ", remark='" + remark + '\'' + '}';
    }


}
