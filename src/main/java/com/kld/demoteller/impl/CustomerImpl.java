package com.kld.demoteller.impl;

import com.kld.demoteller.Account;
import com.kld.demoteller.Customer;
import com.kld.demoteller.CustomerInfo;
import com.kld.demoteller.CustomerNumber;

import java.util.Objects;
import java.util.concurrent.CopyOnWriteArraySet;

public class CustomerImpl implements Customer {

    protected CustomerNumber customerNumber;
    protected String name;
    protected CopyOnWriteArraySet<String> addresses;
    protected CopyOnWriteArraySet<Account> accounts;

    public CustomerImpl(CustomerInfo customerInfo, CustomerNumber customerNumber) {

        this.customerNumber = customerNumber;
        this.name = customerInfo.getName();
        addresses = new CopyOnWriteArraySet<>();
        accounts = new CopyOnWriteArraySet<>();
    }

    @Override
    public CustomerNumber getCustomerId() {

        return customerNumber;
    }

    @Override
    public String getName() {

        return name;
    }

    /**
     * @param address
     * @return
     */
    @Override
    public boolean addAddress(String address) {

        return addresses.add(address);
    }

    @Override
    public CopyOnWriteArraySet<String> getAddresses() {

        return addresses;
    }

    @Override
    public boolean addAccount(Account account) {

        return accounts.add(account);
    }

    @Override
    public CopyOnWriteArraySet<Account> getAccounts() {

        return accounts;
    }

    @Override
    public int hashCode() {

        return Objects.hash(customerNumber, name, addresses, accounts);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof CustomerImpl))
            return false;
        CustomerImpl customer = (CustomerImpl) o;
        return customerNumber.equals(customer.customerNumber) && name.equals(customer.name) &&
               addresses.equals(customer.addresses) && accounts.equals(customer.accounts);
    }

    @Override
    public String toString() {

        return "Customer{" + "customerNumber=" + customerNumber + ", name='" + name + '\'' + ", addresses=" +
               addresses + ", accounts=" + accounts + '}';
    }
}
