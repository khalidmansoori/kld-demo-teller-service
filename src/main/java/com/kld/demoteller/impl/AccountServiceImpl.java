package com.kld.demoteller.impl;

import com.kld.demoteller.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

public class AccountServiceImpl implements AccountService {

    private static final Logger log = LoggerFactory.getLogger(AccountServiceImpl.class);
    protected CustomerService customerService;
    protected AccountNumberGenerator accountNumberGenerator;
    protected ConcurrentHashMap<AccountNumber, Account> accounts;

    public AccountServiceImpl(CustomerService customerService, AccountNumberGenerator accountNumberGenerator) {

        this.customerService = customerService;
        this.accountNumberGenerator = accountNumberGenerator;
        accounts = new ConcurrentHashMap<>();
    }

    /**
     * Creates account based on information received from customer and initial balance;
     *
     * @param amount
     * @param customerInfo
     * @return just created {@link Account} with given accountInfo.
     */
    @Override
    public Account createAccount(BigDecimal amount, CustomerInfo customerInfo) {

        Customer customer = customerService.createORGetCustomer(customerInfo);
        Account newAccount = new AccountImpl(accountNumberGenerator.generate(), amount);
        accounts.put(newAccount.accountNumber(), newAccount);
        customer.addAccount(newAccount);
        log.info("New account created " + newAccount.toString() + " customer " + customer.toString());
        return newAccount;
    }

    /**
     * @param accountNumber id of account we are looking for
     * @return {@code null} if no account were found, or an instance of {@link Account} with given id
     */
    @Override
    public Account accountByNumber(AccountNumber accountNumber) {

        return accounts.get(accountNumber);
    }
}
