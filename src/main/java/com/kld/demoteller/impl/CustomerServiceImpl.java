package com.kld.demoteller.impl;

import com.kld.demoteller.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;

//import io.vertx.ext.web.impl.ConcurrentLRUCache;

public class CustomerServiceImpl implements CustomerService {

    private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);
    protected ConcurrentHashMap<CustomerNumber, Customer> customers;
    protected CustomerNumberGenerator customerNumberGenerator;

    public CustomerServiceImpl(CustomerNumberGenerator customerNumberGenerator) {

        this.customerNumberGenerator = customerNumberGenerator;
        this.customers = new ConcurrentHashMap<>();
    }

    @Override
    public Customer createORGetCustomer(CustomerInfo customerInfo) {
        //TO-Do Support Get as well.
        CustomerNumber customerNumber = customerNumberGenerator.generate();
        Customer customer = new CustomerImpl(customerInfo, customerNumber);
        customers.put(customerNumber, customer);
        log.info("New customer created " + customer.toString());
        return customer;
    }
}
