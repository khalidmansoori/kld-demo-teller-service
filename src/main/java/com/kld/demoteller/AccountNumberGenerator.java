package com.kld.demoteller;

public interface AccountNumberGenerator {
    AccountNumber generate();
}
