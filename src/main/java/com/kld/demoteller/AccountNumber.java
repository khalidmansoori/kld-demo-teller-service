package com.kld.demoteller;

import java.math.BigInteger;
import java.util.Objects;

public class AccountNumber {
    /**
     * Account Id is usually breakage of multiple numbers like branch Id Bank code etc.
     * keeping Long class for simplicity
     */
    private BigInteger accountNumber;

    public AccountNumber(BigInteger accountNumber) {

        this.accountNumber = accountNumber;
    }

    public BigInteger getAccountNumber() {

        return accountNumber;
    }

    @Override
    public int hashCode() {

        return Objects.hash(getAccountNumber());
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof AccountNumber))
            return false;
        AccountNumber that = (AccountNumber) o;
        return Objects.equals(getAccountNumber(), that.getAccountNumber());
    }
}
