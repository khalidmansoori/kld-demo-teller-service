package com.kld.demoteller;

import com.kld.exception.UnsupportedCurrencyException;
import io.vertx.core.json.JsonObject;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;

public class Money {
    private static final String CURRENCY = "currency";
    private static final String AMOUNT = "amount";

    private Currency currency;
    private BigDecimal amount;

    public Money(Currency currency, BigDecimal amount) {

        this.currency = currency;
        this.amount = amount;
    }

    /**
     * @param amount the money.
     */
    public Money(BigDecimal amount) {

        Objects.requireNonNull(amount, "amount cannot be null");
        //TO-DO read from from config or system properties.
        this.currency = Currency.getInstance(Locale.UK);
        this.amount = amount;
    }

    public Money() {
        //TO-DO read from from config or system properties.
        this.currency = Currency.getInstance(Locale.UK);
        this.amount = new BigDecimal("0.0");
    }

    public Currency getCurrency() {

        return this.currency;
    }

    public BigDecimal getAmount() {

        return this.amount;
    }

    //TO-DO hack, ideally we should have a different exception that UnsupportedCurrencyException
    // kept so that in can be used when we start supporting multiple currencies and then add few currency support check.
    public void add(Money money) throws UnsupportedCurrencyException {

        Objects.requireNonNull(money, "money cannot be null");

        // In longer version should delegate to a Helper class for validations.
        if (money.getCurrency() != this.currency)
            throw new UnsupportedCurrencyException(money.getCurrency());

        //TO-DO remove exception and support conversion;
        // Possibly use of ExchangeRateProvider or some micro-service.
        this.amount = this.amount.add(money.getAmount());
    }

    public void subtract(Money money) throws UnsupportedCurrencyException {

        Objects.requireNonNull(money, "money cannot be null");
        if (money.getCurrency() != this.currency)
            throw new UnsupportedCurrencyException(money.getCurrency());

        //TO-DO remove exception and support conversion;
        // Possibly use of ExchangeRateProvider or some micro-service.
        this.amount = this.amount.subtract(money.getAmount());
    }

    public boolean isGreater(Money money) throws UnsupportedCurrencyException {

        Objects.requireNonNull(money, "money cannot be null");
        if (money.getCurrency() != this.currency)
            throw new UnsupportedCurrencyException(money.getCurrency());

        //TO-DO remove exception and support conversion;
        // Possibly use of ExchangeRateProvider or some micro-service.
        return this.amount.compareTo(money.getAmount()) == 1;
    }

    public boolean isLesser(Money money) throws UnsupportedCurrencyException {

        Objects.requireNonNull(money, "money cannot be null");
        if (money.getCurrency() != this.currency)
            throw new UnsupportedCurrencyException(money.getCurrency());

        //TO-DO remove exception and support conversion;
        // Possibly use of ExchangeRateProvider or some micro-service.
        return this.amount.compareTo(money.getAmount()) == -1;
    }

    public boolean isGreaterOrEqual(Money money) throws UnsupportedCurrencyException {

        Objects.requireNonNull(money, "money cannot be null");
        if (money.getCurrency() != this.currency)
            throw new UnsupportedCurrencyException(money.getCurrency());

        //TO-DO remove exception and support conversion;
        // Possibly use of ExchangeRateProvider or some micro-service.
        if (this.amount.compareTo(money.getAmount()) == 0) {
            return true;
        }

        return this.amount.compareTo(money.getAmount()) == 1;
    }

    public boolean isLesserOrEqual(Money money) throws UnsupportedCurrencyException {

        Objects.requireNonNull(money, "money cannot be null");
        if (money.getCurrency() != this.currency)
            throw new UnsupportedCurrencyException(money.getCurrency());

        //TO-DO remove exception and support conversion;
        // Possibly use of ExchangeRateProvider or some micro-service.
        if (this.amount.compareTo(money.getAmount()) == 0) {
            return true;
        }

        return this.amount.compareTo(money.getAmount()) == -1;
    }

    @Override
    public int hashCode() {

        return Objects.hash(currency, amount);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof Money))
            return false;
        Money money = (Money) o;
        return currency.equals(money.currency) && amount.equals(money.amount);
    }

    @Override
    public String toString() {

        return "Money{" + "currency=" + currency + ", amount=" + amount + '}';
    }

    public JsonObject toJson() {

        return new JsonObject().put(AMOUNT, amount.toString()).put(CURRENCY, currency.toString());
    }

}
