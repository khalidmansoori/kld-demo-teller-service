package com.kld.demoteller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;

public interface TransactionRecord {

    /**
     * @return Id of the transactions;
     */
    @NotNull String transactionRecordId();

    /**
     * @return time when the transaction happen
     */
    @NotNull Long timestamp();

    /**
     * @return Details of the transactions;
     */
    @NotNull String description();

    /**
     * @return true if money credited else false.
     */
    boolean isCredit();

    /**
     * @return amount of money debited or  credited.
     */
    @NotNull BigDecimal amount();

    /**
     * @return Balance amount after the transaction;
     */
    @NotNull BigDecimal balance();

    /**
     * @return additional remark if any for this transactions;
     */
    @Nullable String remark();

}
