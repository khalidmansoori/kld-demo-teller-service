package com.kld.demoteller;

import java.math.BigInteger;
import java.util.Objects;

public class CustomerNumber {
    private BigInteger id;

    public CustomerNumber(BigInteger id) {

        this.id = id;
    }

    public BigInteger getId() {

        return id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof CustomerNumber))
            return false;
        CustomerNumber that = (CustomerNumber) o;
        return id.equals(that.id);
    }

    @Override
    public String toString() {

        return "CustomerNumber{" + "id=" + id + '}';
    }
}
