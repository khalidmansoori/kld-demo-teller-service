package com.kld.demoteller;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Represent a transaction.
 */
public interface Transaction {

    /**
     * @return id of the transaction
     */
    String transactionId();

    /**
     * @return account for which money deducted, Can be Bank itself.
     */
    Account debitAccount();

    /**
     * @return account from which balance came , Can be Bank itself.
     */
    Account creditAccount();

    /**
     * @return amount of balance, related to the transaction
     */
    Money getAmount();

    /**
     * @return when timestamps as transcation moves states.
     */
    ConcurrentHashMap<TransactionState, Long> timestamps();

    /**
     * @return details about the transaction for book keeping.
     */
    String remark();

    /**
     * @return state of the transaction
     */
    TransactionState getState();

    /**
     * Set the stat of transactions as it progresses.
     *
     * @param state
     */
    void setState(TransactionState state);

    /**
     * @return result of the transaction
     */
    TransactionResult getResult();

    /**
     * keeps the latest transaction result;
     *
     * @param result
     */
    void setResult(TransactionResult result);


}
