package com.kld.demoteller;

/**
 * Transcation result success or failure with error codes
 * can add more as an when needed.
 * Exceptions would be too much to handle as we grow with more error
 * also this is not exception but an error.
 * But yes unsupported currency is an exception, however as we plan to support more money
 * I m preferring Error code instead of Exception for sake of simplicity as well.
 */

public enum TransactionResult {
    SUCCESS, ACCOUNT_NOT_FOUND, SELF_TRANSFER_FAIL, CREDITOR_ACCOUNT_NOT_FOUND, DEBIT_FAILED_INSUFFICIENT_FUND, DEBITOR_ACCOUNT_NOT_FOUND, DEBIT_FAILED_UNSUPPORTED_CURRENCY, DEBIT_FAILED_INVALID_AMOUNT, CREDIT_FAILED_ACCONUNT_BLOCKED, CREDIT_FAILED_UNSUPPORTED_CURRENCY, CREDIT_FAILED_INVALID_AMOUNT, INTERNAL_SYSTEM_ERROR
}
