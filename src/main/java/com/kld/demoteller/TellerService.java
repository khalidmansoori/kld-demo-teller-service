package com.kld.demoteller;

import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Facade class for performing operations on Bank
 * NOT production ready but suited for demo only.
 */

public interface TellerService {

    /**
     * @param customerInfo
     * @param remark
     * @return AccountNumber
     */
    AccountNumber createAccount(CustomerInfo customerInfo, String remark);

    /**
     * deposit money to account. assuming all of same currency.
     *
     * @param toAccountNumber
     * @param amount
     * @param remark
     * @return TransactionResult
     */

    TransactionResult deposit(BigInteger toAccountNumber, BigDecimal amount, String remark);

    /**
     * Withdraw money from the given account. assuming all of same currency.
     *
     * @param fromAccountNumber
     * @param amount
     * @param remark
     * @return TransactionResult
     */

    TransactionResult withdraw(BigInteger fromAccountNumber, BigDecimal amount, String remark);


    /**
     * Transfers money from the given account to creditor account. assuming all of same currency.
     *
     * @param fromAccountNumber
     * @param toAccountNumber
     * @param amount
     * @param remark
     * @return TransactionResult
     */

    TransactionResult transferMoney(
            BigInteger fromAccountNumber, BigInteger toAccountNumber, BigDecimal amount, String remark);

    /**
     * Return amount present in the account otherwise null if account not found.
     *
     * @param accountNumber
     * @return amount in the account else null.
     */
    @Nullable BigDecimal balance(BigInteger accountNumber);

    @Nullable CopyOnWriteArrayList<TransactionRecord> transactions(BigInteger accountNumber);

    //  boolean deleteAccount(Customer customerInformation);
    //  boolean deleteAccount(BigInteger accountNumber);
}
