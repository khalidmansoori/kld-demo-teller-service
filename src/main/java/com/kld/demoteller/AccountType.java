package com.kld.demoteller;

public enum AccountType {
    LOAN, SAVING, CURRENT
}
