package com.kld.demoteller;

import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Intent: The intent of this class is to capture the customer information
 * for same of simplicity it simply contains name, however in can contains more than that.
 * .eg Contact information, government Id etc.
 * it uniquely defines a customer which can have many different accounts or no account.
 */
public interface Customer {

    /**
     * @return CustomerNumber
     */
    CustomerNumber getCustomerId();

    /**
     * Name of customer string for demo app rather than a class
     */
    String getName();

    /**
     * @param address
     * @return
     */
    boolean addAddress(String address);

    /* List of address of customer string for demo app rather than a class */
    CopyOnWriteArraySet<String> getAddresses();


    boolean addAccount(Account account);

    CopyOnWriteArraySet<Account> getAccounts();
}
