package com.kld.demoteller;

import java.util.concurrent.CopyOnWriteArrayList;

public interface TransactionService {

    /**
     * @return Transaction details of transfer;
     */
    Transaction transfer(Money money, Account debitAccount, Account creditAccount, String remark);

    /**
     * @return all transactions;
     */
    CopyOnWriteArrayList<Transaction> transactions();
}
