package com.kld.terllerservice.cronology;

import java.sql.Timestamp;

/**
 * A physical chronology is simply track record of changes to data
 * instead of delete or update.
 * Idea is, a production data is once created it can be updated or deleted.
 * instead we create newer version of it, since we can be asked anytime in future
 * about the older values or may need to refer to older values.
 * The modifiedAt null means this is the current value.
 */

public interface PhysicalChronology {
    Timestamp insertAt();

    Timestamp modifiedAt();

}
