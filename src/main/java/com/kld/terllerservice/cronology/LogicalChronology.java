package com.kld.terllerservice.cronology;

import java.sql.Timestamp;

/**
 * Logical Chronology class gives validity of a Objects version
 * e.g A Customer information keeps changing over time also customer
 * may ask for future transactions
 * the logical chronology allows to have multiple versions of object and their states
 * given at time.
 * every time there is change in information we create new version instead of updating current on
 * and simply keep valid to as null mean this is still valid.
 * Simple to understand would be keeping Company information of a candidate
 * there would be valid from and valid to and valid to would be null for most recent job :)
 * the precision can be decided later to be seconds or any other lower unit.
 */

public interface LogicalChronology {
    Timestamp validTo();

    Timestamp validFrom();
}
