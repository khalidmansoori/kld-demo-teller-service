# demo-teller-service

A Reactor Pattern based simple Demo purpose basic banking teller REST API's using Eventloop and async communication.

## Getting Started

- This is very basic demo only Application with good code coverage using Junit.
- NOT intended to be used for production, however easy to scale and modify for production.
- Supports as of now only UK Currency however its designed to be easily support more currency.
- The design/code follow SOLID design principles and implemented in different reusable Unit, hence when it is planned to be used for productions, some of code can be reused.

### Scope
- It has limited scope with many assumption for demo.
- Supports Basic account creation, deposit, withdrawal, balance check and transfer of money.
- Accounts have default currency of UK. 
- No validation checks for customers. New account is created for same customer.
- No Authenication of request.
- No API to read transactions, however internal Teller service provides same.
- Has in memory data storage and may not support very large volume of transaction. Everything is Cached.
- Has very basic and minimal validations.
- Basic check for negative scenarios.

### Design
- Design is very simple and modular using clean architecture where database and network is I/O device.
- Hence this does not make any assumption form where the input is coming and where output would be placed.
- Internal data structures like Money, Account, Customer and Services are very modular in nature and holds thier independat identity and test cases.
- Internal TellerService does not assume any source of input hence can be used for any application.
- The internal Services like AccountService Customer service can actaully call REST API or anything by injecting different Service application.
- Easy to fit into any new framework if not using for REST.
- Can be easily build as standalone application.
- Number generations are outsourced to user of applicaiton to be able generate base on use cases. infact in can be a call to microservice.

### Development Environment
- Mac OS 
- IntelliJ IDEA
- Java version "1.8.0_171"

## Technology stack
- [Java8](https://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html)
- [Junit](https://www.vogella.com/tutorials/JUnit/article.html)
- [Vert.x](https://vertx.io/docs/vertx-core/java/)
- [google/gson](https://github.com/google/gson)
- [JetBrains.Annotations](https://www.nuget.org/packages/JetBrains.Annotations)
- [SLF4J](https://www.slf4j.org/manual.html)

## Supported API

####1. Create Account
 `PUT /create-account/{Name}`
 
Example

```
curl -XPUT http://localhost:8080/create-account/Saad
```

Creates new customer Saad and generates an account number.
NOTE: Always creates new Account even if customer is present, there is tradeoff; in real world a customer can have more
than one account, however for demo looks like this may create problem with tracking the new account number.

Output: 
```
{"accountNumber":870819803}
```

```
curl -XPUT http://localhost:8080/create-account/Ayesha 
```
creates new customer Ayesha and generates an account number 
```
Output: 
{"accountNumber":17431231} 
```

####2. Check Balance

`GET /balance/{AccountNumber}`

Returns the balance of amount in given account.

Example
```
curl -XGET http://localhost:8080/balance/870819803
```

Reads balance of customer Saad's account 870819803
Output:

```
{"currency":"GBP","amount":100.0}
```

####3. Withdraw Amount
`POST /withdraw/{amount}/{AccountNumber}`

Example
```
curl -vv -XPOST http://localhost:8080/withdraw/100.01/870819803
```
Withdraws 100.01 from account 870819803


####4. Deposit Amount
`POST /deposit/{amount}/{AccountNumber}`

Example
```
curl -vv -XPOST http://localhost:8080/deposit/1000/870819803
```

####5. Transfer Amount
`POST /transfer/{amount}/{fromAccountNumber}/{ToAccountNumber}`

Example
```
curl -vv -XPOST http://localhost:8080/transfer/100/870819803/17431231
```

Transfers amount 100 from Saad to Ayesha.

## Build

Building jar with all dependencies

```
./gradlew jar
```

Packages demo-teller-service.jar at build/libs/

## Run
```
java -jar build/libs/demo-teller-service.jar
```
Starts the Http server on port 8080.

```
java -Dcom.kld.demoteller.port={PORT} -jar build/libs/demo-teller-service.jar
```
Starts the Http server on port user defined port.

## Test

Handy scripts
```
./run_server.sh [PORT]
```
Once you see the build is successfull and Server running.

run shell script that calls REST API via curl command.
```
./test/test-rest-api.sh [PORT]
```

## Concurrency Test

run shell script that calls REST API via curl command. Calls deposit on account with 100 parallel request and then checks the balance. The results are attached in logs

```
./test/test-rest-api-concurrency.sh [PORT]
```
In progress work concurrency test, more test cases can be added later however current test good to demontsrate the power of event loop
and async execution.

## Automated Test coverage Junit
- Test Coverge and Junit Test cases are also avilable, need to update intrusction for same. Have been running it inside IntelliJ IDE but need to check how to run from gradle. like I use gmock test_runner.

## Log
See attached log file in logs directory

### Developer Note
- The Application is done very quickly within few days of efforts and within constraints.
- This is a implemented by a C++ developer with some hands on experience on Core Java, NO hands on experience with Java based micro-services and REST API frameworks like springboot, Jetty or Jersy.
- This is first learning app which has in memory data storage and may not support large volume of transaction.

#### Why Async and Event loop.
I have good experiance of having project running on single event loop and asyncronous api's. The thread are cooperating with each others
rather than fighting. The high level of concurrancy can be achived with minimum number of threads.
The code running on event loop ideally works like a single process with no blocking call hence runs very fast and scalable.

### Why Vert.x
Eclipse Vert.x is event driven and non blocking. This means your app can handle a lot of concurrency using a small number of kernel threads. 
- Vert.x lets your app scale with minimal hardware.
- Vert.x is lightweight - Vert.x core is around 650kB in size.
- Vert.x is not an application server. There's no monolithic Vert.x instance into which you deploy applications. You just run your apps wherever you want to.
- Vert.x is modular - when you need more bits just add the bits you need and nothing more.
- Vert.x is simple but not simplistic. Vert.x allows you to create powerful apps, simply.
- Vert.x is an ideal choice for creating light-weight, high-performance, microservices.

Intrestingly I was working on simler project on C++ designing for event-loop but never completed, when I thought of Http based server
I found this what I always wanted to write.

### Missing
- Good coverage for test cases.
- Mocking of services and depedancy injection.
- [Mockito](https://site.mockito.org/) Pending to use in Junit test cases.
- Notificaton machasim.
- Logging Aspect and sources data for REST API.
- Validators
- A proper configuration for various hard coding.

### TO-DO 
- Use different packaging for services, model and REST Api handler.
- Implment Command pattern for REST API handlers and change object level invocations rather than functional level.
- Improver REST API specs.
- Use of Observer design for notification of transactions.
- Abstract Factory and Factory use for Object creation.
- A much better number generator for production.
- Apply best practices of Java.
- Rounds of code reviews.
- User better mocking of class for regirous Unit testing.
- Add more coverage in unit testing.
- Many more...
- Add API to read transaction, aka account statments.
- Build CLI for ability to use any rest api from command line

### Additional Info

About main Concurrency framework with Event loop.

Reactor and Multi-Reactor

Vert.x APIs are event driven - Vert.x passes events to handlers when they are available.

In most cases Vert.x calls your handlers using a thread called an event loop.

As nothing in Vert.x or your application blocks, the event loop can merrily run around delivering events to different handlers in succession as they arrive.

Because nothing blocks, an event loop can potentially deliver huge amounts of events in a short amount of time. For example a single event loop can handle many thousands of HTTP requests very quickly.

We call this the Reactor Pattern.

Vert.x works differently here. Instead of a single event loop, each Vertx instance maintains several event loops. By default we choose the number based on the number of available cores on the machine, but this can be overridden.

Verticles are chunks of code that get deployed and run by Vert.x. A Vert.x instance maintains N event loop threads (where N by default is core*2) by default.
