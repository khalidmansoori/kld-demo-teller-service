#! /bin/bash
./gradlew jar || { echo 'Build failed to creat demo-teller-service.jar' ; exit 1; }
echo $result
java -Dcom.kld.demoteller.port=$1 -jar build/libs/demo-teller-service.jar
